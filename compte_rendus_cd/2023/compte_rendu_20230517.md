
# Compte rendu point CD AFPy



- [https://git.afpy.org/AFPy/gestion/src/branch/master/compte\_rendus\_cd/2023]([]https://git.afpy.org/AFPy/gestion/src/branch/master/compte\_rendus\_cd/2023[])

- [https://bbb.afpy.org/b/jul-cb2-1bd-ost](https://bbb.afpy.org/b/jul-cb2-1bd-ost)



## 17 mai 2023



**Début prévu : 20h00**



Présents :

- [ ] Marc Debureaux

- [x] Lucie Anglade

- [ ] Thomas Bouchet

- [ ] Bruno Bonfils

- [ ] Pierre Bousquié

- [ ] Antoine Rozo

- [x] Laurine Leulliette

- [x] Jean Lapostolle

- [ ] Nicolas Ledez



- [ ] Hervé Sousset

- [x] JeffD

- [ ] Julien Palard

- [ ] Haxoul



## Informations

- l'AFPY peut se féliciter de ses  échanges bienveillants et de son organisation dans les réunions parce personnellement je vois beaucoup d'assos/collectifs où c'est pas le cas 💛  💚   💙  💜





## Actions prévues lors de la réunion précédente



   * Thomas : se renseigner pour savoir si l’association peut avoir le statut d’intérêt général-> osp
   * debnet : Prendre contact avec gandi pour l'interview et obtenir les questions -> osp
   * Lucie : Écrire un article récapitulatif sur le site de la pycon suite aux résultats du sondage -> attends de relecture avant de publier
   * Antoine : finaliser le compte-rendu + déclaration de l'AGO → attente de réponse de la préfecture (update mai : toujours pas de nouvelle)
   * Pierre : se renseigner auprès d'une maison d'asso sur les aspects juridiques concernant la réception de donation / rémunération d'intervenant·e·s -> osp
   * Pierre relancer l’université pour le paiement/facture de la sécurité -> osp






## Ordre du jour

   * **[]PyconFR à Strasbourg[]** - Toussaint 2024: 
           * On a l'accord de principe de nouveau directeur qui est très intéressé (vu qu'il a utilisé Python si j'ai bien compris). On aurait du Jeudi 31 octobre 2024 au dimanche 3 novembre. Il faut savoir que le créneau de 2023 a déja été réservé pour la Toussaint
           * La FAC n'a qu'une semaine de vacances et les cours reprendraient le lundi 4 novembre
           * (à savoir) elle privilégie les demandes de location venant de personnes (prof/etudiantes) en internes
           * La direction MathInfo va négocier pour obtenir l'amphi l'amphi EOST (100 places). On aurait 2 amphis (300 et 250) et une salle de 90places - Et des salles de 40 places. (Toutes ont un vidéoprojecteur)
   * Quel outil pour gérer l’organisation de la PyConFR 2024 ? (discuss avec la vue board [https://discuss.afpy.org/c/19/l/latest?board=default](https://discuss.afpy.org/c/19/l/latest?board=default), les tickets sur gitea si y a une vue board…)
   * Quel outil pour gérer les recherches sur certains sujets ? (post wiki sur le discuss, framapad…)




   * **AG durant la Pycon** - Est-ce que légalement on a le droit de diffuser l'AG (audio, conférence Signal ou BBB) pour que deux responsables de l'asso puissent rester au niveau de l'accueil et qu'on soit plus réactif en cas de soucis avec le public ?
   * **[]Prochaine réunion[]**: c'est le 21 juin (fête de la musique) - On peut faire un rapide sondage pour déplacer au 14juin (si on veut garder mercredi) ? : LE 20 JUIN


## Actions

   * JeffD : Recontacter Laura et/ou le directeur pour faire une demande interne
   * JeffD : Partager des infos sur le lieux l'Université sur discuss
   * Melcore : Étudier la possibilité de faire un board avec Gitea 
   * ReiNula : Boite à idée sur le discuss pour récupérer des idées pour la PyConFR et une boite à idée physique durant la PyConFr




Prochaine réunion le 20 juin à 20h00



**Fin : 20h53**


