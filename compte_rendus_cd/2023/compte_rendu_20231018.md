
# Compte rendu point CD AFPy



- [https://git.afpy.org/AFPy/gestion/src/branch/master/compte\_rendus\_cd/2023]([]https://git.afpy.org/AFPy/gestion/src/branch/master/compte\_rendus\_cd/2023[])

- [https://bbb.afpy.org/b/jul-cb2-1bd-ost](https://bbb.afpy.org/b/jul-cb2-1bd-ost)



## 18 octobre 2023



**Début prévu : 20h00**



Présents :

- [x] Marc Debureaux

- [x] Lucie Anglade

- [ ] Thomas Bouchet

- [ ] Bruno Bonfils

- [x] Pierre Bousquié

- [x] Antoine Rozo

- [ ] Laurine Leulliette

- [ ] Jean Lapostolle

- [ ] Nicolas Ledez



- [x] Hervé Sousset

- [x] Jeff D

- [x] Julien Palard



## Informations



## Actions prévues lors de la réunion précédente



   * debnet : Prendre contact avec Gandi pour l'interview et obtenir les questions → reporté en novembre
   * Pierre : se renseigner auprès d'une maison d'asso sur les aspects juridiques concernant la réception de donation / rémunération d'intervenant·e·s -> Relancé le 16/10/23, pas de réponse
   * JeffD : Partager des infos (plan, accès) sur le lieux l'Université sur discuss → fait
   * ReiNula : Boite à idée sur le discuss pour récupérer des idées pour la PyConFR et une boite à idée physique durant la PyConFr → a commencé à rédiger et à rechercher pour une boite pliable -> pas avancée (pas d'urgence car cela doit être prêt pour la PyCon)
   * Jeff : Faire la demande d'inscription/convention pour la toussaint 2024 auprès de l'université de Strasbourg -> fait (Laura)
   * Lucie \& Mindiell : Lucie repasse sur la refonte du site pour le CSS et Mindiell fera une PR après → en cours (Une PR a été faite, il semblerait que cela ait bien avancé) [https://git.afpy.org/Mindiell/refonte/pulls/1](https://git.afpy.org/Mindiell/refonte/pulls/1)
   * Marc : transmettre mots de passe notebooks \& helloasso à Lucie (dans pass)-> Pas fait
   * Marc : envoyer les documents concernant la demande d'inscription pour l'université -> plus nécessaire
   * Lucie : Reprendre le site PyConFr 2020 pour faire la version 2024 (en masquant les onglets non finalisés) -> en cours [https://github.com/AFPy/pyconfr-2024](https://github.com/AFPy/pyconfr-2024)
       * Faire des demandes de devis pour avoir une idée des montants à demander aux sponsors -> Le sujet avance
   * Antoine : Contacter Thierry pour mise en place d'un atelier mixteen à la PyConFR 2024 → Recontacté récemment, on devait s'appeler pour en parler
   * Lucie : prendre contact pour signer les conférences ([http://www.sils-interpretes.fr/tarifs/](http://www.sils-interpretes.fr/tarifs/) par exemple) -> le devis est à 4725€ / salle / jour - (En français et suisse romand). Le prestataire de 2019 avait couté ~4000€
       * Attention à la difficulté de compréhensions du signage.
       * Demander à Amaury qui a un projet là dessus.
   * Pierre : reprendre le budget 2023 pour prédire le budget 2024 (+sur nourriture? +/- sur captation?) + reprendre l'historique des sponsors (Timeline)
       * La compta 2023 a été bouclée
       * [https://discuss.afpy.org/t/pyconfr-2023-bilan-previsionnel/1174/15](https://discuss.afpy.org/t/pyconfr-2023-bilan-previsionnel/1174/15)
   * Lucie : Mail à Framasoft pour leur demander s'ils voudraient donner une conférence -> relancés, pas de nouvelle
   * Julien, Pierre et Nicolas : relancer les sponsors => préparer un PAD pour se répartir les sponsors -> PAD fait
   * Est-ce que l'on change de prestataire pour les petits déjeuners ? Est-ce que l'on ré-ajuste les quantités en fonction du nombre de participants ? Melcore : trouver des prestataires et les comparer (par rapport aux paniers de Léa)
   * Julien: La boîte mail Gandi va expirer, il faut leur demander de nous en faire cadeau. -> géré




## Ordre du jour



## Actions

   * Marc : contacter les paniers de Léa pour évaluer les tarifs 2024


## Actions reportées indéfiniment

   * Thomas : se renseigner pour savoir si l’association peut avoir le statut d’intérêt général
   * Lucie : créer un ticket pyconfr-orga sur le discuss pour y déplacer les actions liées


Prochaine réunion le 15 novembre à 20h00



**Fin : 20h25**
