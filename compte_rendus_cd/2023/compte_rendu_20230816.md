
# Compte rendu point CD AFPy



- [https://git.afpy.org/AFPy/gestion/src/branch/master/compte\_rendus\_cd/2023]([]https://git.afpy.org/AFPy/gestion/src/branch/master/compte\_rendus\_cd/2023[])

- [https://bbb.afpy.org/b/jul-cb2-1bd-ost](https://bbb.afpy.org/b/jul-cb2-1bd-ost)



## 16 août 2023



**Début prévu : 20h00**



Présents :

- [ ] Marc Debureaux

- [x] Lucie Anglade

- [X] Thomas Bouchet

- [ ] Bruno Bonfils

- [x] Pierre Bousquié

- [x] Antoine Rozo

- [x] Laurine Leulliette

- [ ] Jean Lapostolle

- [ ] Nicolas Ledez



- [ ] Hervé Sousset

- [x] Jeff D

- [ ] Julien Palard



## Informations



## Actions prévues lors de la réunion précédente



   * Thomas : se renseigner pour savoir si l’association peut avoir le statut d’intérêt général → pas avancé
   * debnet : Prendre contact avec gandi pour l'interview et obtenir les questions → reporté en septembre
   * Pierre : se renseigner auprès d'une maison d'asso sur les aspects juridiques concernant la réception de donation / rémunération d'intervenant·e·s -> en attente
   * JeffD : Partager des infos (plan, accès) sur le lieux l'Université sur discuss → à voir fin août à la réouverture de l'université (vérifier accès fauteuils)
   * ReiNula : Boite à idée sur le discuss pour récupérer des idées pour la PyConFR et une boite à idée physique durant la PyConFr → a commencé à rédiger et à rechercher pour une boite pliable
   * Faire la demande d'inscription/convention pour la toussaint 2024 auprès de l'université de Strasbourg -> en attente (Jeff)
   * Lucie \& Mindiell : Lucie repasse sur la refonte du site pour le CSS et Mindiell fera une PR après → en cours
   * Lucie : Ouvrir un sujet discuss pour se coordonner en septembre à Paris (rdv Gandi, repas ?)
   * Laurine : Ajouter le point ecocup à la liste des choses à prévoir pour la prochaine pycon → après vérification, point déjà inscrit dans le sujet prévu






## Ordre du jour

   * PV d'AGO validé par la préfecture \o/ [https://git.afpy.org/AFPy/gestion/pulls/2](https://git.afpy.org/AFPy/gestion/pulls/2)
   * Sponsors pour la PyConFR 2024 (cf [https://discuss.afpy.org/t/pyconfr-2024-recherche-des-sponsors/1679)](https://discuss.afpy.org/t/pyconfr-2024-recherche-des-sponsors/1679)) :
       * À partir de quand on communique sur le fait de chercher des sponsors ?
       * Est-ce qu’il faudrait pas un site pour pouvoir communiquer sur les différents paliers et mettre les logos au fur et à mesure ? Un site de PyConFR pas full fini ? (Quid du design)
       * Est-ce que les tarifs changent suite à l'inflation ?
       * Est-ce qu'en plus des paliers "classiques" on rajouterait pas des sponsoring spécifiques qu’une entreprise peut choisir ? (Crêche / garderie, personnes pour signer)
=> Existe-t-il une limite au nombre de sponsors ? (limite de place pour l'Université)

=> Les tarifs sont conservés

=> Première version du site (sans les onglets)

   * Utilisation du site 2020 (Strasbourg annulé) par Lucie
=> Devis pour crêche / garderie / p'tits déjs

   * CloudBuilders ([https://www.cloud-builders.tech/en/python-conf)](https://www.cloud-builders.tech/en/python-conf)) — meetup en ligne gratuit, qui nous demandent du support
       * ok pour communiquer (tweet + toot + linkedin) au nom de l'afpy sur leur événement


[]Facultatif[]

- Quelle place pour nos soutiens informels: exemple Framasoft pour ses services, ou du soutien matériel d'association locales ?

   * => partenaires affichés sous les sponsors sur le site, pour mettre en valeur les personnes/entités qui nous soutiennent sans apport financier


## Actions

   * Marc : transmettre mots de passe notebooks \& helloasso à Lucie (dans pass)
   * Marc : envoyer les documents concernant la demande d'inscription pour l'université
   * Lucie : Reprendre le site PyConFr 2020 pour faire la version 2024 (en masquant les onglets non finalisés)
       * Faire des demandes de devis pour avoir une idée des montants à demander aux sponsors
   * Antoine : Contacter Thierry pour mise en place d'un atelier mixteen à la PyConFR 2024
   * Lucie : prendre contact pour signer les conférences ([http://www.sils-interpretes.fr/tarifs/](http://www.sils-interpretes.fr/tarifs/) par exemple)
   * Pierre : reprendre le budget 2023 pour prédire le budget 2024 + reprendre l'historique des sponsors
   * Lucie : Répondre à CloudBuilders
   * Lucie : Mail à Framasoft pour leur demander s'ils voudraient donner une conférence




Prochaine réunion le 20 septembre à 20h00



**Fin : 20h45**
