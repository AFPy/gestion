
# Compte rendu point CD AFPy



- [https://git.afpy.org/AFPy/gestion/src/branch/master/compte\_rendus\_cd/2023]([]https://git.afpy.org/AFPy/gestion/src/branch/master/compte\_rendus\_cd/2023[])

- [https://bbb.afpy.org/b/jul-cb2-1bd-ost](https://bbb.afpy.org/b/jul-cb2-1bd-ost)



## 15 novembre 2023



**Début prévu : 20h00**



Présents :

- [x] Marc Debureaux

- [x] Lucie Anglade

- [ ] Thomas Bouchet

- [ ] Bruno Bonfils

- [x] Pierre Bousquié

- [ ] Antoine Rozo

- [x] Laurine Leulliette

- [ ] Jean Lapostolle

- [ ] Nicolas Ledez



- [x] Hervé Sousset

- [ ] Jeff D

- [ ] Julien Palard



## Informations



## Actions prévues lors de la réunion précédente



   * debnet : Prendre contact avec Gandi pour l'interview et obtenir les questions → voir une occasion pour aller sur Paris et coupler avec Gandi
   * Pierre : se renseigner auprès d'une maison d'asso sur les aspects juridiques concernant la réception de donation / rémunération d'intervenant·e·s -> Relancé le 16/10/23, pas de réponse -> Toujours pas de réponse (au 15/11/23)
   * Lucie \& Mindiell : Lucie repasse sur la refonte du site pour le CSS et Mindiell fera une PR après → en cours (Une PR a été faite, il semblerait que cela ait bien avancé) [https://git.afpy.org/Mindiell/refonte/pulls/1](https://git.afpy.org/Mindiell/refonte/pulls/1)
   * Marc : transmettre mots de passe notebooks \& helloasso à Lucie (dans pass)-> Pas fait, noooon :(
   * Antoine : Contacter Thierry pour mise en place d'un atelier mixteen à la PyConFR 2024 → Il est intéressé et doit voir si d'autres bénévoles mixteen seraient intéressés
   * Lucie : Mail à Framasoft pour leur demander s'ils voudraient donner une conférence -> relancés, pas de nouvelle ([https://discuss.afpy.org/t/pyconfr-2024-framasoft/1792)](https://discuss.afpy.org/t/pyconfr-2024-framasoft/1792))
   * Marc : contacter les paniers de Léa pour évaluer les tarifs 2024 → [https://discuss.afpy.org/t/pyconfr-2024-le-petit-dejeuner/1793](https://discuss.afpy.org/t/pyconfr-2024-le-petit-dejeuner/1793) -> appel fait, les Paniers de Léa doivent nous recontacter, peut-être les relancer




## Ordre du jour

   * Garder le site de la PyConFR 23 : on garde le site actuel de 2023 en rajoutant une actu et texte sur la page d’accueil pour teaser 2024
   * Convention pour l’université : pas de news, relancer Laura


## Actions

   * Lucie : mise à jour du site de la PyConFR 2023
   * Lucie : contacter Laura pour la convention avec l’université
   * Lucie : rétro-planning avec les étapes principales de la PyConFR, ticket sur le Discuss, tout le monde participe hein !


## Actions reportées indéfiniment

   * Thomas : se renseigner pour savoir si l’association peut avoir le statut d’intérêt général


Prochaine réunion le 20 décembre à 20h00



**Fin : 20h26**
