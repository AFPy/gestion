
# Compte rendu point CD AFPy



- [https://github.com/AFPy/afpy\_gestion/tree/master/compte\_rendus\_cd/2023](https://github.com/AFPy/afpy\_gestion/tree/master/compte\_rendus\_cd/2023)

- [https://bbb.afpy.org/b/jul-cb2-1bd-ost](https://bbb.afpy.org/b/jul-cb2-1bd-ost)



## 11 Janvier 2023



**Début prévu : 20h00**



Présents :

- [x] Marc Debureaux
- [] Jules Lasne
- [x] Lucie Anglade
- [x] Thomas Bouchet
- [] Bruno Bonfils
- [x] Pierre Bousquié
- [] Antoine Rozo
- [x] Laurine Leulliette
- [x] Jean Lapostolle



- [x] Hervé Sousset
- [] JeffD
- [x] Julien Palard
- [] Florian Guillet
- [] Marcos Medrano





## Actions prévues lors de la réunion précédente



   * Antoine/Marc : Faire un post sur discuss pour établir une liste des exigences que l'on a pour des locaux PyConFr (matériel nécessaire, disponibilités en calendrier, nombre de places, etc.). Au plus fort de la journée à Bordeaux on avait 666 personnes (moins qu'à Lille où on avait dépassé les 800). Partager avec les universités un document formel incluant ces exigences et des photos des événements. → en cours pour le moment rien
   * Pierre : Faire la paperasse pour le transfert de « propriétaire » à la banque et sur paypal. ← J’imagine qu’il faudra de la paperasse style PV d’AG  +dépot préfecture + Carte d’identités + patte blanche et petit four à Lyon -> A tenter sans le dépôt préfecture  → FAIT avec succès
   * Thomas : se renseigner pour savoir si l’association peut avoir le statut d’intérêt général
   * debnet : Prendre contact avec gandi pour l'interview et obtenir les questions
   * Pierre : Prendre contact avec Hévéa pour la gestion du courrier et savoir ce qui est envisageable → en cours
   * - Pierre : Regarder les factures des années précédentes pour voir à quels imprimeurs on faisait appel et quels sont les tarifs habituels
   * Pierre : Récupérer les modèles de lettres d'invitations envoyées en 2019 et vérifier quelles infos étaient demandées
   * Antoine : Vérifier la légitimité des demandes d'invitations à la PyConFr (speakers ? inscrit·e·s ?) et envoyer les invitations si nécessaires après récolte des infos nécessaires (nom complet, numéro d'identité, etc.)
   * Équipe comm : relancer le cycle général de comm (twitter / mastodon / linkedin / discord / etc.) autout de la participation à l'événement et pour les speakers
   * Lucie : envoyer message au groupe meetup Lyon (~1500 personnes)
   * Marc : idem sur le meetup Lille mais semble un peu mort
   * Antoine : Prendre contact avec les foodtrucks si personne n'est sur le coup
   * Marc : contacter les Paniers de Léa
   * CD : voter pour les confs entre le 7 et le 11 janvier pour qu'on puisse se décider lors de la réunion du 11
   * Jeff : voir pour imprimer des affiches PyConFr et Afpy pour la fosdem qui aura lieu début février




## Ordre du jour

   * Qui fait le programme ? (je suis chô − Lucie)
   * Règles de sélection des confs / sprints / ateliers (en plus de suivre les votes bien sûr, pour maximiser le nombre de speakers) : nombres de talk (on évite qu’une personne ait plein de talks), les personnes qui viennent de loin, détails sur la présentation du talk
   * Une limite sur le nombre de talks en anglais ou pas ? : 1,5 tracks anglophones, bien étiquetter en anglais / en français
   * Combien de sprints possibles niveau place ?  : il y a plein de places, on peut sûrement tout accepter
   * Est-ce qu’on sélectionne des sprints en anglais même si ils ne sont pas bien classés après les votes (je n’ai pas encore regardé ce que ça donnait) pour que les participants non-francophones aient le choix dans les sprints ? : y a plein de places, osef
   * Comment ça se passe la communication pour informer les gens si leur sujet est retenu ou non ? (au moment de la création du planning ?) (chô aussi − Lucie) : quand on clique sur accepter ça envoit un mail, on peut test sur nos conférences. Dire aux gens d’en parler et rappeler qu’il faut s’inscrire sur HelloAsso
   * Liste des salles pour les sprints du jeudi/vendredi, liste des salles pour les ateliers, liste des salles pour les conférences (et le nombre de places) : c’est pareil que la précédente édition
   * Est-ce qu’on met des plénières ? Si oui, quels talks on met en plénière ? : une plénière bienvenue le samedi matin, une clôture pour le dimanche soir (si possible), un créneau pour les sponsors (dans la plénière de bienvenue), pas de plénières car on a pas demandé avant
   * Un créneau Lightning Talks ? : OUI!!!!
   * Convocation à l’AG : à faire
   * Point sur les bourses : [https://discuss.afpy.org/t/suivi-demande-de-bourses/1173](https://discuss.afpy.org/t/suivi-demande-de-bourses/1173) : attention à la limite (le 15 janvier)
   * Point sur le budget : [https://discuss.afpy.org/t/bilan-previsionnel-pycon-2023/1174](https://discuss.afpy.org/t/bilan-previsionnel-pycon-2023/1174) : on est plutôt bon, même en payant des bourses à toutes les demandes
   * Demande par mail de gens qui veulent faire des confs mais se sont réveillés trop tard dimanche → leur proposer de faire un lightning talk
   * Point sur les t-shirts : quelle quantité / tailles Et quel surplus pour de l'achat sur place ? : pour les tailles il faut voir avec le prestataire, idem pour les quantités (200 max)
   * Point sur les badges : on rachète quelque chose ou pas ?
       * Staff (50aine), Volunteer (50aine), Speaker, Sponsor
           * un peu de tout, arrondi à la centaine supérieure
       * badges plastiques : 200
   * Point sur les tours de cou : On achète tout ? Si oui, combien ?
       * 1000
   * Est-on assez de bénévoles ? : jamais assez
   * Heure pour récup le buffet du samedi avec le traiteur et pour rendre le matériel (verres et écocup) : mettre une caution sur les verres car c’est 1€ par pièce qui manque sinon
   * Le comité diversité : Laurine et Lucie (au minimum)


Taipy :présentation de 2 minutes en pleinière, possiblité d'avoir un stand permanent.







# Stats d'inscrits



[https://pad.chapril.org/p/inscriptions-pycon-fr-2023](https://pad.chapril.org/p/inscriptions-pycon-fr-2023)   <= pas à jour ? Si si julien le met à jour "de temps en temps".



## Actions

   * Lucie : fait le programme avec Guillaume
   * Marc : envoyer la convocation avant le 19 janvier
   * Jean : Répondre à Taipy
   * Âme charitable : rappeler à Marc si il a pas fait de convocation d’ici le 16 janvier
   * Jean : Retardataire : Leur proposer de faire un Lightning Talk ou trou dans le planning
   * Mindiell : les t-shirts
   * Marc : voir avec Yoann pour le retour du matériel du buffet
   * Laurine et Lucie : voir l’orga pour le comité diversité


Prochaine réunion le 26 janvier à 20h00



**Fin : 21h39 **