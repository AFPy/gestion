
# Compte rendu point CD AFPy



- [https://git.afpy.org/AFPy/gestion/src/branch/master/compte\_rendus\_cd/2023]([]https://git.afpy.org/AFPy/gestion/src/branch/master/compte\_rendus\_cd/2023[])

- [https://bbb.afpy.org/b/jul-cb2-1bd-ost](https://bbb.afpy.org/b/jul-cb2-1bd-ost)



## 20 juin 2023



**Début prévu : 20h00**



Présents :

- [X] Marc Debureaux

- [X] Lucie Anglade

- [X] Thomas Bouchet

- [ ] Bruno Bonfils

- [X] Pierre Bousquié

- [X] Antoine Rozo

- [X] Laurine Leulliette

- [X] Jean Lapostolle

- [ ] Nicolas Ledez



- [X] Hervé Sousset

- [X] JeffD

- [X] Julien Palard



## Informations

- l'AFPY peut se féliciter de ses  échanges bienveillants et de son organisation dans les réunions parce personnellement je vois beaucoup d'assos/collectifs où c'est pas le cas 💛  💚   💙  💜





## Actions prévues lors de la réunion précédente



   * Thomas : se renseigner pour savoir si l’association peut avoir le statut d’intérêt général → pas avancé
   * debnet : Prendre contact avec gandi pour l'interview et obtenir les questions → ils doivent revenir vers nous
   * Lucie : Écrire un article récapitulatif sur le site de la pycon suite aux résultats du sondage → fait (en ligne)
   * Antoine : finaliser le compte-rendu + déclaration de l'AGO → refusé, qui est partant pour une grève de la faim devant la préfecture de Lyon ? (avec de la rosette alors ?)
   * Pierre : se renseigner auprès d'une maison d'asso sur les aspects juridiques concernant la réception de donation / rémunération d'intervenant·e·s -> Pas eu plus d’info. RDV reporté à ce Jeudi \o/.
   * Pierre relancer l’université pour le paiement/facture de la sécurité -> Fait et payé \o/!
   * JeffD : Recontacter Laura et/ou le directeur pour faire une demande interne -> Laura participera ✨ en tant qu'organisatrice pour la PyConFR Strasbourg (requis par l'université)
   * JeffD : Partager des infos sur le lieux l'Université sur discuss → à faire
   * Melcore : Étudier la possibilité de faire un board avec Gitea -> Oui [https://git.afpy.org/Melcore/PyConTest/projects/4](https://git.afpy.org/Melcore/PyConTest/projects/4) <- WOW oo!
   * ReiNula : Boite à idée sur le discuss pour récupérer des idées pour la PyConFR et une boite à idée physique durant la PyConFr






## Ordre du jour

   * Invitation EuroPython → qui à l'AFPy voudrait profiter d'invitations gratuites à l'EuroPython en juillet ?
   * Proposer à Vincent \& Yoan
   * Création réseau meetups by Artur → faire un post discuss pour présenter la plateforme
   * Gandi et ses changements → augmentation des tarifs des boîtes mail chez Gandi (normalement ça ne devrait rien changer pour nous, mais à surveiller et rester prêt à bouger si besoin)
   * Site AfPy.org - bascule en mode statique ou pas (Mindiell) → plutôt pour
   * Mobilizon (alternative à meetup) : expérience en cours (en cours d'installation) pour tester le produit
   * Les gens de Strasbourg seraient partant pour remplacer meetup par mobilizon
   * est-ce que la catégorie meetup de discours pourrait être suffisante → pas très user-friendly
   * meetup permet de ramener des gens qui ne suivent pas spécialement l'asso
   * trouver des villes cobayes pour chacun des outils (Strasbourg : mobilizon, Paris : discuss)




## Actions

   * Marc : transmettre mots de passe notebooks \& helloasso à Lucie (dans pass)
   * Antoine : refaire la déclaration du PV de l'AGO en corrigeant la date, explicitant que la composition du bureau est décrite dans le règlement intérieur (à joindre à la déclaration) et croiser les doigts
   * Faire la demande d'inscription/convention pour la toussaint 2024 auprès de l'université de Strasbourg
   * Lucie : proposer à Yoan et Vincent pour l’europython
   * Lucie : post discuss pour le groupe de meetups by Artur
   * Lucie \& Mindiell : Lucie repasse sur la refonte du site pour le CSS et Mindiell fera une PR après




Prochaine réunion le 19 juillet à 20h00



**Fin : xxhxx**
