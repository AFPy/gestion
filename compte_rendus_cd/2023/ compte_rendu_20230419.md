
# Compte rendu point CD AFPy



- [https://git.afpy.org/AFPy/gestion/src/branch/master/compte\_rendus\_cd/2023]([]https://git.afpy.org/AFPy/gestion/src/branch/master/compte\_rendus\_cd/2023[])

- [https://bbb.afpy.org/b/jul-cb2-1bd-ost](https://bbb.afpy.org/b/jul-cb2-1bd-ost)



## 19 avril 2023



**Début prévu : 20h00**



Présents :

- [x] Marc Debureaux

- [x] Lucie Anglade

- [ ] Thomas Bouchet

- [ ] Bruno Bonfils

- [x] Pierre Bousquié

- [x] Antoine Rozo

- [x] Laurine Leulliette

- [x] Jean Lapostolle

- [x] Nicolas Ledez



- [ ] Hervé Sousset

- [ ] JeffD

- [x] Julien Palard

- [x] Haxoul





## Actions prévues lors de la réunion précédente



   * Thomas : se renseigner pour savoir si l’association peut avoir le statut d’intérêt général
   * debnet : Prendre contact avec gandi pour l'interview et obtenir les questions
   * Lucie : Écrire un article récapitulatif sur le site de la pycon suite aux résultats du sondage
   * Lucie : mettre les compte-rendus de diversité sur gitea → fait
   * Pierre : mettre les bilans financiers dans le compte-rendu de l’AG → fait
   * Antoine : finaliser le compte-rendu + déclaration de l'AGO → attente de réponse de la préfecture
   * Melcore : Ouvrir un sujet-wiki sur discuss pour répertorier le matériel qu'on a suite à la pycon (adaptateurs, stickers, etc.) → fait ([https://discuss.afpy.org/t/pyconfr-2024-materiel-restant-des-annees-precedentes/1424)](https://discuss.afpy.org/t/pyconfr-2024-materiel-restant-des-annees-precedentes/1424))






## Ordre du jour

   * Sondage PyConFR terminé, rapport écrit, ne reste plus qu'à publier
   * Hébergement fiscal de projets.
       * Rémunération de personnes travaillant sur des projets de l'asso
       * Donation reçue de la PSF pour Julien/hackinscience, qui peut-on rémunérer avec ça si c'est l'AFPy qui reçoit la donation ? Possibilité de payer une prestation à court-bouillon ? (compliqué puisque Lucie est au bureau de l'association)
       * L'AFPy peut-elle même recevoir cette donation ?
       * Deadline pour HackInScience le 14 septembre
   * PyConFR 2024 dans 1 an et demi, on fait quoi entre deux ? (En attendant la PyConFR)
       * Strasbourg recontactée pour 2024, changement directement, RDV demain avec Jeff pour en discuter
       * Les dernières éditions d'« En attendant la PyConFR » (pendant le covid) ne rassemblaient pas beaucoup de monde
       * Tenter de changer le format (les conditions du covid qui contraignaient à des confs en ligne ne sont plus là) ? Travail sur un projet commun ?
       * + de meetups locaux ? → motiver les gens et les inciter à participer à la "vraie" PyConFR / renforcer+pérenniser les meetups existants, en lancer de nouveaux (Clermont ?)
       * Communication à faire autour du breizh camp


## Actions

   * Lucie : publier l’article sur le site
   * Pierre : se renseigner auprès d'une maison d'asso sur les aspects juridiques concernant la réception de donation / rémunération d'intervenant·e·s
   * Pierre relancer l’université pour le paiement/facture de la sécurité




Prochaine réunion le 17 mai à 20h00



**Fin : 20h44**


