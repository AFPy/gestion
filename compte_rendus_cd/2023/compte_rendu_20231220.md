
# Compte rendu point CD AFPy



- [https://git.afpy.org/AFPy/gestion/src/branch/master/compte\_rendus\_cd/2023]([]https://git.afpy.org/AFPy/gestion/src/branch/master/compte\_rendus\_cd/2023[])

- [https://bbb.afpy.org/b/jul-cb2-1bd-ost](https://bbb.afpy.org/b/jul-cb2-1bd-ost)



## 20 décembre 2023



**Début prévu : 20h00**



Présents :

- [x] Lucie Anglade

- [ ] Marc Debureaux

- [ ] Thomas Bouchet

- [ ] Bruno Bonfils

- [ ] Pierre Bousquié

- [x] Antoine Rozo

- [ ] Laurine Leulliette

- [ ] Jean Lapostolle

- [ ] Nicolas Ledez



- [x] Hervé Sousset

- [ ] Jeff D

- [ ] Julien Palard



## Informations



## Actions prévues lors de la réunion précédente



   * debnet : Prendre contact avec Gandi pour l'interview et obtenir les questions → voir une occasion pour aller sur Paris et coupler avec Gandi
   * Pierre : se renseigner auprès d'une maison d'asso sur les aspects juridiques concernant la réception de donation / rémunération d'intervenant·e·s -> Relancé le 16/10/23, pas de réponse -> Toujours pas de réponse (au 15/11/23)
   * Lucie \& Mindiell : Lucie repasse sur la refonte du site pour le CSS et Mindiell fera une PR après → en cours (Une PR a été faite, il semblerait que cela ait bien avancé) [https://git.afpy.org/Mindiell/refonte/pulls/1](https://git.afpy.org/Mindiell/refonte/pulls/1)
   * Marc : transmettre mots de passe notebooks \& helloasso à Lucie (dans pass)-> Fait
   * Lucie : Mail à Framasoft pour leur demander s'ils voudraient donner une conférence -> relancés, pas de nouvelle ([https://discuss.afpy.org/t/pyconfr-2024-framasoft/1792)](https://discuss.afpy.org/t/pyconfr-2024-framasoft/1792))
   * Marc : contacter les paniers de Léa pour évaluer les tarifs 2024 → [https://discuss.afpy.org/t/pyconfr-2024-le-petit-dejeuner/1793](https://discuss.afpy.org/t/pyconfr-2024-le-petit-dejeuner/1793) -> appel fait, les Paniers de Léa doivent nous recontacter, peut-être les relancer
   * Lucie : mise à jour du site de la PyConFR 2023 → pas nécessaire
   * Lucie : contacter Laura pour la convention avec l’université → attente de réponse de Laura
   * Lucie : rétro-planning avec les étapes principales de la PyConFR, ticket sur le Discuss, tout le monde participe hein !




## Ordre du jour

   * Campagne annuelle de dons Framasoft
       * Veut-on faire un don au nom de l'AFPy?
   * HackInScience x Code Lutin [https://discuss.afpy.org/t/hackinscience-recoit-1-6k-de-code-lutin/1849](https://discuss.afpy.org/t/hackinscience-recoit-1-6k-de-code-lutin/1849)
       * Communiquer sur le soutien
   * Retours sur le meetup en mixité choisie à Lyon
       * 18 personnes, bonne ambiance, dont la moitié des gens qui ne font pas de Python de base mais sont venues pour le principe de la mixité choisie
       * Essayer d'en faire 3-4 dans l'année pour permettre à ces personnes de venir, en gardant un sujet Python + sujet pas Python pour rester ouvert
       * Un seul retour négatif (d'un non-participant) sur Linkedin après l'événement pour râler sur la mixité choisie
   * Invitation au JRES 2024
       * [https://www.jres.org/](https://www.jres.org/) à Rennes (couvent des jacobins) du 10 au 13 décembre 2024
       * Conférence axée logiciel libre qui nous propose d'y tenir un stand, voir si des gens seraient intéressés (potentiellement Hervé et Nicolas, il faudrait au moins 2 personnes pour tenir le stand 4j)
   * Revue du rétroplanning
       * [https://discuss.afpy.org/t/pyconfr-2024-retroplanning/](https://discuss.afpy.org/t/pyconfr-2024-retroplanning/)


## Actions

   * Julien : Mettre en place la redirection pycon.fr vers pycon.fr/2024
   * Comm : Communiquer sur le fait qu'on commence à organiser la PyConFR 2024 (quand la redirection sera en place)
   * Lucie : Ouvrir un sujet sur le discuss pour discuter de la campagne de dons framasoft et savoir si on a de quoi donner
   * Antoine : Pinger la comm (Melcore, Laurine) sur le post de Julien concernant HackInScience × Code Lutin pour communiquer dessus → fait
   * Lucie : Ouvrir un sujet pour sonder le CD sur le meilleur créneau pour organiser les réunions CD


## Actions reportées indéfiniment

   * Thomas : se renseigner pour savoir si l’association peut avoir le statut d’intérêt général


Prochaine réunion le 17 janvier à 20h00



**Fin : 20hXX**
