
# Compte rendu point CD AFPy



- [https://git.afpy.org/AFPy/gestion/src/branch/master/compte\_rendus\_cd/2023]([]https://git.afpy.org/AFPy/gestion/src/branch/master/compte\_rendus\_cd/2023[])

- [https://bbb.afpy.org/b/jul-cb2-1bd-ost](https://bbb.afpy.org/b/jul-cb2-1bd-ost)



## 19 juillet 2023



**Début prévu : 20h00**



Présents :

- [x] Marc Debureaux

- [x] Lucie Anglade

- [ ] Thomas Bouchet

- [x] Bruno Bonfils

- [ ] Pierre Bousquié

- [x] Antoine Rozo

- [x] Laurine Leulliette

- [x] Jean Lapostolle

- [x] Nicolas Ledez



- [ ] Hervé Sousset

- [] JeffD

- [ ] Julien Palard



## Informations



## Actions prévues lors de la réunion précédente



   * Thomas : se renseigner pour savoir si l’association peut avoir le statut d’intérêt général → pas avancé
   * debnet : Prendre contact avec gandi pour l'interview et obtenir les questions → à voir à partir de septembre (première réponse où ils demandent une personne qui viendrait à Paris, pour que l'interview soit plus interactive et filmée), mais sont preneurs pour un RDV
   * Pierre : se renseigner auprès d'une maison d'asso sur les aspects juridiques concernant la réception de donation / rémunération d'intervenant·e·s -> en attente
   * JeffD : Partager des infos sur le lieux l'Université sur discuss → à faire
   * ReiNula : Boite à idée sur le discuss pour récupérer des idées pour la PyConFR et une boite à idée physique durant la PyConFr → pas avancé, se renseigner sur des boîtes physiques pliables
   * Marc : transmettre mots de passe notebooks \& helloasso à Lucie (dans pass) → à faire
   * Antoine : refaire la déclaration du PV de l'AGO en corrigeant la date, explicitant que la composition du bureau est décrite dans le règlement intérieur (à joindre à la déclaration) et croiser les doigts → nouvelle déclaration faite (30/06/2023), en attente d'un retour
   * Faire la demande d'inscription/convention pour la toussaint 2024 auprès de l'université de Strasbourg -> en attente (Jeff)
   * Lucie : proposer à Yoan et Vincent pour l’europython → proposé mais pas de réponse
   * Lucie : post discuss pour le groupe de meetups by Artur → fait
   * Lucie \& Mindiell : Lucie repasse sur la refonte du site pour le CSS et Mindiell fera une PR après → à faire






## Ordre du jour

   * Laurine : Est-ce que ça vaut le coup de faire autant d'ecocups pour l'année en cours lors des PyConFR? On pourrait en avoir des standard AFPy à réutiliser d'année en année
       * Possibilité aussi de les louer puis les rendre à la fin (mais pas de choix sur les visuels) : [https://strasbourg.envie.org/page-d-exemple/nos-activites/location-de-gobelets/](https://strasbourg.envie.org/page-d-exemple/nos-activites/location-de-gobelets/)




## Actions

   * Lucie : Ouvrir un sujet discuss pour se coordonner en septembre à Paris (rdv Gandi, repas ?)
   * Laurine : Ajouter le point ecocup à la liste des choses à prévoir pour la prochaine pycon




Prochaine réunion le 16 août à 20h00



**Fin : 20h25**
