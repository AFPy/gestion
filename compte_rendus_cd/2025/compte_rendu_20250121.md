
# Compte rendu point CD AFPy



- [https://git.afpy.org/AFPy/gestion/src/branch/master/compte\_rendus\_cd/2025](https://git.afpy.org/AFPy/gestion/src/branch/master/compte\_rendus\_cd/2025)

- [https://meet.evolix.org/afpy-cd](https://meet.evolix.org/afpy-cd)



## 21 janvier 2025



**Début prévu : 20h30**



Présents :

- [x] Lucie Anglade

- [x] Marc Debureaux

- [ ] Thomas Bouchet

- [x] Pierre Bousquié

- [x] Antoine Rozo

- [x] Laurine Leulliette

- [x] Agnès Haasser

- [x] Julie Rymer

- [x] Paul Guichon



- [x] Julien Palard

- [ ] Hervé Sousset





## Informations



## Actions prévues lors de la réunion précédente

   * Pierre : regarder si on peut upgrade le compte Meetup pour créer plus de groupes avec un tarif association -> Mail en cours (au 16/07/24), rien de nouveau au 21/01
   * Antoine : Écrire une page sur le site de l'AFPy pour présenter le CD -> à faire
   * Lucie : Demander à Pierre quelles finances on a pour les dons à des assos -> trésorerie présente, demander à Framasoft pour le don (et le montant)
   * Antoine : Redemander les adresses postales + professions des personnes du CD pour la déclaration en préfecture → fait
   * Pierre : Payer la bonne facture du Tigre (-2116,50€) → fait
   * Pierre : Payer la dernière bourse (-400€) → fait
   * Pierre : Encaisser la caisse (+892.26€) → pas encore fait
   * Pierre : Répartir l’argent entre paypal et socgen. → pas encore fait
   * Pierre : ... payer babychou?... enfin après résolution du tarif... (-300?€) → appels en cours par Mindiell
   * Pierre : Payer Raffut ... en attente de facture. (env -4000€) → facture toujours en attente
   * Pierre : Faire corriger la faute d’orthographe sur le journal-officiel. → pas fait
   * Pierre : Publier les photos sur [https://photos.afpy.org/](https://photos.afpy.org/) → photos travaillées mais pas encore publiées (playbook ansible dédié)
   * Lucie : regarder les tarifs pour les stickers sur StickerApp → pas fait
   * Agnès / Mindiell : regarder pour les polos sur [https://enventelibre.org](https://enventelibre.org) (ou autre site) → regardé sur tunetoo.com (site qui s'occupe de faire les commandes à la demande sans gestion de stock) mais il faut encore passer du temps pour choisir (tshirt, tasses, polos ?), discussion à suivre sur le discuss
   * Mindiell : rappeler Babychou (pour pas que Lucie s'énerve) → fait plusieurs fois
   * Pierre : Faire un don de 1000 euros à Framasoft (don unique 1 fois hein!) → pas fait, mettre la même phrase que celle de présentation de l'asso pour apparaître en tant que mécène




## Ordre du jour

   * Où en est-on concernant la validation du PV d'AG ?
       * À valider dans la semaine
   * Quid du compte twitter de l'AFPy / PyConFR
       * Y a-t-il intérêt  rejoindre bluesky ?
       * On a déjà un compte mastodon sur l'instance de la quadrature [https://mamot.fr/@AFPy](https://mamot.fr/@AFPy)
       * C'est bien d'avoir un réseau de prédilection, mais intéressant d'être présent partout pour être vu
       * Annoncer départ de twitter
   * Migration des serveurs / mails terminée, DNS en cours (renouvellement aux dates d'échéance)
       * On pourrait contacter Cédric pour l'informer que la migration s'était bien passée
   * Captation vidéo : 2 vidéos avec des problèmes de son (les intervenant·e·s vont être contacté·e·s) et plusieurs slides manquantes (intervenant·e·s à contacter aussi)
       * Voir avec Raffut pour partager le lien même si toutes les vidéos ne sont pas encore présentes
       * D'abord contacter les intervenant·e·s (après le retour de Raffut), puis dans le mail de bonne année


## Actions

   * Laurine : préparer le post pour quitter Twitter (compte AFPy et PyConFR)
   * Lucie : envoyer un message à Cédric
   * Lucie : poster / envoyer les messages de bonne année
   * Marc : lire le PV de l’AG
   * Marc : créer sujet sur Europython sur le discuss




## Actions reportées indéfiniment

   * Thomas : se renseigner pour savoir si l’association peut avoir le statut d’intérêt général
   * Pierre : se renseigner auprès d'une maison d'asso sur les aspects juridiques concernant la réception de donation / rémunération d'intervenant·e·s -> Relancé le 16/10/23, pas de réponse -> Toujours pas de réponse (au 20/02/24)
   * Marc : Ajouter le pointage des adhérent·e·s de l'asso (pour l'AGO) dans l'outil de pointage PyConFR


Prochaine réunion le 18 février à 20h30



**Fin : 21h17**
