
# Compte rendu point CD AFPy



- [https://git.afpy.org/AFPy/gestion/src/branch/master/compte\_rendus\_cd/2025](https://git.afpy.org/AFPy/gestion/src/branch/master/compte\_rendus\_cd/2025)

- [https://meet.evolix.org/afpy-cd](https://meet.evolix.org/afpy-cd)



## 18 février 2025



**Début prévu : 20h30**



Présents :

- [x] Lucie Anglade

- [x] Marc Debureaux

- [ ] Thomas Bouchet

- [x] Pierre Bousquié

- [x] Antoine Rozo

- [x] Laurine Leulliette

- [ ] Agnès Haasser — absence prévue

- [x] Julie Rymer

- [x] Paul Guichon



- [ ] Julien Palard

- [ ] Hervé Sousset





## Informations



## Actions prévues lors de la réunion précédente

   * Pierre : regarder si on peut upgrade le compte Meetup pour créer plus de groupes avec un tarif association -> Mail en cours (au 16/07/24), rien de nouveau au 18/02
   * Antoine : Écrire une page sur le site de l'AFPy pour présenter le CD -> sujet ouvert sur discuss, PR à suivre
   * Pierre : Encaisser la caisse (+892.26€) → fait
   * Pierre : Répartir l’argent entre paypal et socgen. → pas encore fait
   * Pierre : ... payer babychou?... enfin après résolution du tarif... (-300?€) → rien de neuf (pas de nouveau message)
   * Pierre : Payer Raffut ... en attente de facture. (env -4000€) → facture toujours en attente
   * Pierre : Faire corriger la faute d’orthographe sur le journal-officiel. → pas fait
   * Pierre : Publier les photos sur [https://photos.afpy.org/](https://photos.afpy.org/) → photos travaillées mais pas encore publiées (playbook ansible dédié)
   * Lucie : regarder les tarifs pour les stickers sur StickerApp → rectangulaires 5×3 30€ les 50, carrés 5×5 39€ les 50 ou 29€ les 30, livraison gratuite
   * Agnès / Mindiell : regarder pour les polos sur [https://enventelibre.org](https://enventelibre.org) (ou autre site) → regardé sur tunetoo.com (site qui s'occupe de faire les commandes à la demande sans gestion de stock) mais il faut encore passer du temps pour choisir (tshirt, tasses, polos ?), discussion à suivre sur le discuss
   * Pierre : Faire un don de 1000 euros à Framasoft (don unique 1 fois hein!) → don fait et mail envoyé
   * Laurine : préparer le post pour quitter Twitter (compte AFPy et PyConFR) → en cours (post image commencé sur penpot), reciter tous les canaux sur lesquels on est présents
   * Lucie : envoyer un message à Cédric → fait
   * Lucie : poster / envoyer les messages de bonne année → fait, tout a bien marché avec le nouveau service d'emails
   * Marc : lire le PV de l’AG → fait
   * Marc : créer sujet sur Europython sur le discuss
   * Antoine : déclaration de l'AGO en préfecture → fait (en attente de réponse)




## Ordre du jour

   * Mail Rolkem
       * Recherche de contact de gens ayant travaillé sur un progiciel en 2015 pour faire évoluer un projet qui avait été monté en collaboration avec des gens de l'AFPy.
       * Demander plus de détails et indiquer de poster l'annonce sur discuss
   * [Antoine] J'ai plus accès aux documents déposés sur service-public.fr 😭
       * Voir pour se synchroniser sur le code d'activation, et peut-être changer l'email de connexion au site pour ne plus rencontrer de problème
   * Publication des videos sur dl.afpy.org ?
       * A-t-on accès aux fichiers vidéos ?
   * PyConFR 2025 : trouver le lieu pour avancer sur le retroplanning


## Actions

   * Lucie : demander la facture à Raffut, et les infos sur les vidéos manquantes
   * Lucie : répondre à Rolkem
   * Antoine: Demander à Julien pour savoir comment ça se passe pour l'upload des vidéos sur dl.afpy.org
   * Lucie : faire un rétroplanning
   * Pierre : payer Annabelle




## Actions reportées indéfiniment

   * Thomas : se renseigner pour savoir si l’association peut avoir le statut d’intérêt général
   * Pierre : se renseigner auprès d'une maison d'asso sur les aspects juridiques concernant la réception de donation / rémunération d'intervenant·e·s -> Relancé le 16/10/23, pas de réponse -> Toujours pas de réponse (au 20/02/24)
   * Marc : Ajouter le pointage des adhérent·e·s de l'asso (pour l'AGO) dans l'outil de pointage PyConFR


Prochaine réunion le 18 mars à 20h30



**Fin : 21h10**
