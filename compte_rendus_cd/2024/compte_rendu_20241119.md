
# Compte rendu point CD AFPy



- [https://git.afpy.org/AFPy/gestion/src/branch/master/compte\_rendus\_cd/2024]([]https://git.afpy.org/AFPy/gestion/src/branch/master/compte\_rendus\_cd/202[]4)

- [https://meet.evolix.org/afpy-cd](https://meet.evolix.org/afpy-cd)



## 19 novembre 2024



**Début prévu : 20h30**



Présents :

- [x] Lucie Anglade

- [x] Marc Debureaux

- [x] Thomas Bouchet

- [ ] Pierre Bousquié

- [x] Antoine Rozo

- [x] Laurine Leulliette

- [ ] Agnès Haasser

- [x] Julie Rymer

- [x] Paul Guichon



- [x] Julien Palard

- [ ] Hervé Sousset





## Informations



## Actions prévues lors de la réunion précédente

   * Pierre : regarder si on peut upgrade le compte Meetup pour créer plus de groupes avec un tarif association -> Mail en cours (au 16/07/24)
   * Melcore : Contenu affiche -> todo (pas prio) (pour les JRES / Journée Réseau de l'Enseignement Supérieur)
       * Finalement pas de participation aux JRES (pas d'engagement à tenir un stand car pas sûrs d'être là)
   * Marc : demander à ta femme pour du sponsoring :D -> en attente de réponse (relancé), conditionné à leur budget (toujours pas de réponse à ce jour) → trop tard (pas de réponse), voir pour 2025
   * Melcore : Écrire une page sur le site de l'AFPy pour présenter le CD (pas prio) -> en cours
   * Hervé : savoir la deadline pour envoyer les nombres de personnes au JRES → plus nécessaire
   * Antoine : corriger les liens github→gitea sur le site de l'AFPy → PR ouverte et mergée, déployée
   * Antoine : rouvrir un sujet récurrent pour les réunions CD, après la PyConFR → fait
   * Lucie : contacter la responsable de l'atelier pour les enfants pour annoncer que c'est trop tard → fait
   * Lucie : donner les droits helloasso à Antoine pour accéder à la liste des adhérent·e·s → fait
   * Marc : communiquer à Julien la liste des membres du CD et du bureau → fait




## Ordre du jour

   * Pour la prochaine AGO, faire pointer les adhérent·e·s à l'entrée dans la salle
   * Le matériel restant ([https://discuss.afpy.org/t/pyconfr-2025-materiel-restant-des-annees-precedentes/1424)](https://discuss.afpy.org/t/pyconfr-2025-materiel-restant-des-annees-precedentes/1424))
   * Tour des réponses au sondage
       * Environ 100 réponses (1/3 des participant·e·s)
       * Dont 50% pour qui c'était la 1ère PyConFR
       * Les gens sont globalement satisfaits par l'événement
       * Public plutôt défavorable (à 52%) à l'idée d'une keynote non-Python
       * Il faudrait mieux flécher l'événement, avoir des conférences de niveaux plus divers et bien catégoriser les niveaux
       * Faire une FAQ sur les points qui reviennent régulièrement dans les sondages de satisfaction (notamment la langue des conférences)
       * Peut-être prévoir une salle un peu plus ouverte, "chill", pour se reposer
       * Communiquer sur le fait qu'on accueille aussi les conférences non-techniques ou pour débutant·e·s, qu'il y a un public pour ça, idem pour les conférences niveau expert
       * Intégrer le jobboard à la todo-list pour la prochaine PyConFR
   * Des gens à relancer pour avoir les factures ? À voir avec le trésorier
   * Mail de Strype pour présenter leur IDE à notre asso
       * Poster sur le discuss de l'AFPy, organiser un webinaire; un sujet de meetup, etc...
       * Proposer une conférence pour l'édition suivante
   * Campagne soutien framasoft ([https://soutenir.framasoft.org/fr/)](https://soutenir.framasoft.org/fr/))
       * Don au nom de l'AFPy ? On utilise leurs outils (framapad)
       * Prévoir un vote en AG sur les assos que l'on veut supporter
   * Migration serveurs / boites mails / noms de domaine
       * Les boîtes mail arrivent à expiration, penser à garder des copies des emails
       * Octopuce seraient contents et ok pour nous héberger, juste à trouver l'adminsys, iels sont assez fans de Python
       * Côté mail on a le go de Galae, à voir comment ajuster les volumes pour les envois de masse, les boîtes seront opérationnelles cette semaine et une migration DNS sera à prévoir
       * Pour les noms de domaine, lebureau.coop est ok pour qu'on migre chez eux, en cours de test sur un domaine perso de Julien. Mais on aura besoin d'une API DNS-01 pour le renouvellement des certificats, à voir pour un plugin certbot
       * On pourra ensuite les citer / ajouter leurs logos sur le site de l'AFPy
   * Compte-rendu d'AGO prêt à être déposé ?
       * Pull-request ouverte : [https://git.afpy.org/AFPy/gestion/pulls/8](https://git.afpy.org/AFPy/gestion/pulls/8)


## Actions

   * Antoine : Reprendre l'action de Melcore sur la page de présentation du CD
   * Marc : Ajouter le pointage des adhérent·e·s de l'asso (pour l'AGO) dans l'outil de pointage PyConFR
   * Lucie : Demander à Pierre quelles finances on a pour les dons à des assos
   * Lucie : répondre à Strype
   * Antoine : Redemander les adresses postales + professions des personnes du CD pour la déclaration en préfecture






## Actions reportées indéfiniment

   * Thomas : se renseigner pour savoir si l’association peut avoir le statut d’intérêt général
   * debnet : Prendre contact avec Gandi pour l'interview et obtenir les questions → voir une occasion pour aller sur Paris et coupler avec Gandi
   * Pierre : se renseigner auprès d'une maison d'asso sur les aspects juridiques concernant la réception de donation / rémunération d'intervenant·e·s -> Relancé le 16/10/23, pas de réponse -> Toujours pas de réponse (au 20/02/24)


Prochaine réunion le 17 décembre à 20h30



**Fin : 21h45**
