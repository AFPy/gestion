
# Compte rendu point CD AFPy



- [https://git.afpy.org/AFPy/gestion/src/branch/master/compte\_rendus\_cd/2024]([]https://git.afpy.org/AFPy/gestion/src/branch/master/compte\_rendus\_cd/202[]4)

- [https://meet.evolix.org/afpy-cd](https://meet.evolix.org/afpy-cd)



## 20 août 2024



**Début prévu : 19h30**



Présents :

- [x] Lucie Anglade

- [ ] Marc Debureaux

- [ ] Thomas Bouchet

- [ ] Bruno Bonfils

- [ ] Pierre Bousquié

- [x] Antoine Rozo

- [x] Laurine Leulliette

- [ ] Jean Lapostolle

- [ ] Nicolas Ledez



- [x] Hervé Sousset

- [x] Jeff D

- [ ] Julien Palard



## Informations



## Actions prévues lors de la réunion précédente

   * Pierre : regarder si on peut upgrade le compte Meetup pour créer plus de groupes avec un tarif association -> Mail en cours (au 16/07/24)
   * Melcore : créer le sujet dans le Discuss pour l’appel à volontaire -> en cours, sera fait rapidement
   * Pierre : Revoir le stock de goodies/tourdecoup/anciens t-shirts -> Retrouvé, pas fait les comptes
   * Melcore : Contenu affiche -> todo (pour les JRES)
   * Hervé : ouvrir sujet dans Association pour location logement pendant la PyConFR -> fait
   * Antoine: contacter les foodtrucks → fait. On en a 2 confirmés (avec option vegan) et 2 en option.
   * Antoine : aider Jeff pour trouver des associations de captation vidéo -> redirigé vers un prestataire, pas de nouvelles
   * Pierre : chercher des prestataires pour faire des devis pour les t-shirts -> Plusieurs mais un peu tôt pour fin septembre.  Prix entre 10-15€TTC
   * Marc : la pub sur ReadTheDocs [https://docs.readthedocs.io/en/stable/advertising/ethical-advertising.html#community-ads](https://docs.readthedocs.io/en/stable/advertising/ethical-advertising.html#community-ads) -> faire pub pour l'événement (trop tard pour le cfp)
   * Lucie : rajouter sur HelloAsso / le site que l’on rembourse pas les trucs payés après la conférence -> fait
   * Marc : demander à ta femme pour du sponsoring :D -> en attente de réponse, conditionné à leur budget
   * Marc: Faire une interface propre en amont pour la gestion du pointage à l'entrée de la PyConFR
   * Lucie: Redemander à l'université si on peut condamner une des deux entrées
   * Antoine: Faire la demande à la DALI Université de Strasbourg pour les foodtrucks -> fait, à relancer début septembre
   * Laurine / Melcore : Comm pour relancer les sponsors d'ici fin août -> à faire
   * Laurine / Melcore : Comm de dernière minute 48h avant la fin du CFP (vendredi) -> fait
   * Hervé : Ouvrir un sujet dans pyconfr-orga concernant les nappes AFPy pour divers événements -> fait
   * Melcore : Écrire une page sur le site de l'AFPy pour présenter le CD




## Ordre du jour

   * Foodtrucks
   * La sécurité : faut voir avec la personne de la sécurité si c’est OK le plan et si on peut bien fermer la porte sud (ce qui ferait un vigile en moins)
   * Sponsors : au moins un nouveau bronze et un or
   * Bourses
   * Lettres d’invatation : 3 pour l’instant
   * Inscriptions : 32 sprints, 53 conf / ateliers, 24 soirée. 1 espace enfants le dimanche
   * Planning :
       * 8 sprints (4 en attente de confirmation)
       * 54 talks hors keynotes (16 en attente de confirmation)
       * 9 ateliers (4 en attente de confirmation)
       * Il y a une track en anglais pour les talks et une pour les ateliers. 3 ateliers en parallèle (sauf pendant les lightning talks où y a que ça)
       * en attente : détails de l’atelier enfants
   * Mettre des réunions un peu plus rapprochées avant la PyConFR après celle de septembre ? Genre 8 et 22 octobre ? : OK
   * Affiche sistech ([https://sis.tech/france/)](https://sis.tech/france/)) : OK
   * PyData Paris x AFPy : OK sur le fait qu’ils peuvent déjà le faire en fait
   * JRES : Hervé x Nicolas x une autre de Rennes
   * Badges différenciants pour l’orga à la PyConFR ? : imprimer sur une feuille de couleurs pour l’orga




## Actions

   * Antoine: relancer DALI début septembre
   * Laurine / Melcore : Comm pour relancer les sponsors d'ici fin août






## Actions reportées indéfiniment

   * Thomas : se renseigner pour savoir si l’association peut avoir le statut d’intérêt général
   * debnet : Prendre contact avec Gandi pour l'interview et obtenir les questions → voir une occasion pour aller sur Paris et coupler avec Gandi
   * Pierre : se renseigner auprès d'une maison d'asso sur les aspects juridiques concernant la réception de donation / rémunération d'intervenant·e·s -> Relancé le 16/10/23, pas de réponse -> Toujours pas de réponse (au 20/02/24)


Prochaine réunion le 17 septembre à 19h30



**Fin : 20h04**
