# Compte rendu point CD AFPy



- [https://git.afpy.org/AFPy/gestion/src/branch/master/compte\_rendus\_cd/2024]([]https://git.afpy.org/AFPy/gestion/src/branch/master/compte\_rendus\_cd/202[]4)

- [https://meet.evolix.org/afpy-cd](https://meet.evolix.org/afpy-cd)



## 18 juin 2024



**Début prévu : 19h30**



Présents :

- [x] Lucie Anglade

- [x] Marc Debureaux

- [ ] Thomas Bouchet

- [ ] Bruno Bonfils

- [x] Pierre Bousquié

- [ ] Antoine Rozo

- [x] Laurine Leulliette

- [ ] Jean Lapostolle

- [ ] Nicolas Ledez



- [x] Hervé Sousset

- [x] Jeff D

- [ ] Julien Palard



## Informations



## Actions prévues lors de la réunion précédente

   * Marc : contacter les paniers de Léa pour évaluer les tarifs 2024 → [https://discuss.afpy.org/t/pyconfr-2024-le-petit-dejeuner/1793](https://discuss.afpy.org/t/pyconfr-2024-le-petit-dejeuner/1793) -> on a un devis avec TOUT, en attente du devis de Purcoop (?) pour comparer
   * Lucie : contacter l’université → salles conférences OK. Attente retour pour les autres salles + hall de la part de l’université et process autorisation de foodtrucks
   * Pierre : regarder si on peut upgrade le compte Meetup pour créer plus de groupes avec un tarif association -> Mail en cours (au 16/06/24)
   * Lucie : contacter la Place Des Grenouilles pour la PyConFR ->retour fin août. À voir si on peut les défrayer si on a plein de sous
   * Marc : transmettre article en anglais contact PyConUS -> à formater en mode "discord", mentionner l’event / cfp. Limite 6000 caractères
   * Melcore : créer le sujet dans le Discuss pour l’appel à volontaire -> en cours
   * Pierre : Revoir le stock de goodies/tourdecoup/anciens t-shirts -> Retrouvé \o/. A trier.
   * Melcore : Contenu affiche -> todo
   * Marc : envoyer la demande à la PSF -> c’est fait \o/ il faudra envoyer la liste des speakers / titre / abstract du programme, voire juste la liste des propositions quand le CFP est fermé (demande de 8k$)
   * Hervé : ouvrir sujet dans Association pour location logement pendant la PyConFR -> un peu plus proche de la date de la PyConFR
   * Antoine: contacter les foodtrucks → en cours, doit relancer et en contacter de nouveaux
   * Antoine : aider Jeff pour trouver des associations de captation vidéo -> email envoyé
   * Lucie : contacter une nouvelle personne pour la keynote EN -> c’est bon on a une keynote en anglais
   * Pierre : retrouver le nombre de t-shirts vendus par taille -> fait [https://discuss.afpy.org/t/pyconfr-2024-les-goodies/2124](https://discuss.afpy.org/t/pyconfr-2024-les-goodies/2124)
   * Pierre : chercher des prestataires pour faire des devis pour les t-shirts -> Plusieurs mais un peu tôt pour fin octobre.  Ou fait-on livrer? Prix entre 10-15€TTC
   * Lucie : ouvrir sujet pour les goodies -> done




## Ordre du jour

   * Faire une « pub » pour la PyConFR à publier sur [https://docs.readthedocs.io/en/stable/advertising/index.html](https://docs.readthedocs.io/en/stable/advertising/index.html)
   * Les sous -> [https://discuss.afpy.org/t/pyconfr-2024-les-sponsors/1689/14?u=pierre.bousquie](https://discuss.afpy.org/t/pyconfr-2024-les-sponsors/1689/14?u=pierre.bousquie)
   * La garderie / espace enfants
       * info sur le site
       * inscription helloasso
       * com dessus
   * Prix pour les t-shirts et la soirée
       * t-shirts : 10-15€
       * soirée : Le Tigre -> voir si possible d’avoir un traiteur végan en plus
       * soirée : The Social Bar -> demander devis
       * soirée : on reste sur du 30€ max, à voir si on dépasse pas avec les devis
   * De la com pour reparler du cfp ?
       * refaire un post en "olala il reste qu’un mois 😱"
       * faire des posts pour présenter les différents formats (ateliers, sprints, conférences)
   * PyLadies -> idem que 2018 (déjeuner financé par l’AFPy et limité ~40personnes sur inscription)




## Actions

   * Marc : rework le message de com pour le Discord Python pour annoncer la PyConFR et le CFP
   * Lucie : demander à l’université si on peut faire livrer les t-shirts là-bas, sinon chez Jeff ou si on trouve une personne de l’université
   * Marc : la pub sur ReadTheDocs [https://docs.readthedocs.io/en/stable/advertising/ethical-advertising.html#community-ads](https://docs.readthedocs.io/en/stable/advertising/ethical-advertising.html#community-ads)
   * Laurine : com pour la garderie / espace enfants
   * Lucie : rajouter sur HelloAsso / le site que l’on rembourse pas les trucs payés après la conférence
   * Jeff : Le Tigre et The Social Bar
   * Laurine : posts pour le CFP (rappel et présentation des différents formats)
   * Marc : demander à ta femme pour du sponsoring :D




## Actions reportées indéfiniment

   * Thomas : se renseigner pour savoir si l’association peut avoir le statut d’intérêt général
   * debnet : Prendre contact avec Gandi pour l'interview et obtenir les questions → voir une occasion pour aller sur Paris et coupler avec Gandi
   * Pierre : se renseigner auprès d'une maison d'asso sur les aspects juridiques concernant la réception de donation / rémunération d'intervenant·e·s -> Relancé le 16/10/23, pas de réponse -> Toujours pas de réponse (au 20/02/24)


Prochaine réunion le 16 juillet à 19h30



**Fin : 20h38**
