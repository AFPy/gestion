# Compte rendu point CD AFPy



- [https://git.afpy.org/AFPy/gestion/src/branch/master/compte\_rendus\_cd/2024]([]https://git.afpy.org/AFPy/gestion/src/branch/master/compte\_rendus\_cd/202[]4)

- [https://meet.evolix.org/afpy-cd](https://meet.evolix.org/afpy-cd)



## 3 novembre 2024



**Début prévu : 12h30**



Présents :

- [x] Lucie Anglade

- [x] Marc Debureaux

- [x] Thomas Bouchet

- [x] Pierre Bousquié

- [x] Antoine Rozo

- [x] Laurine Leulliette

- [ ] Agnès Haasser

- [x] Julie Rymer

- [x] Paul Guichon



- [ ] Hervé Sousset

- [ ] Jeff D





## Informations



## Actions prévues lors de la réunion précédente

   * Pierre : regarder si on peut upgrade le compte Meetup pour créer plus de groupes avec un tarif association -> Mail en cours (au 16/07/24)
   * Melcore : Contenu affiche -> todo (pas prio) (pour les JRES)
   * Marc : demander à ta femme pour du sponsoring :D -> en attente de réponse (relancé), conditionné à leur budget (toujours pas de réponse à ce jour)
   * Marc: Faire une interface propre en amont pour la gestion du pointage à l'entrée de la PyConFR -> fait
   * Melcore : Écrire une page sur le site de l'AFPy pour présenter le CD (pas prio) -> en cours
   * Hervé : savoir la deadline pour envoyer les nombres de personnes au JRES
   * Antoine : contacter les paniers de Léa pour voir si on peut faire quelque chose pour l'anniversaire -> hors budget
       * Voir ce qu'il est possible (brownie sans noix ? flan ? tartes ?) et pour combien de personnes (150 ?)
   * Laurine : faire affiche concernant l'équipe diversité -> fait
   * Lucie : prendre un abonnement pour 1 mois de numéro de téléphone proxy pour la PyConFR -> fait
   * Antoine : corriger les liens github→gitea sur le site de l'AFPy → PR ouverte et mergée, à déployer
   * Antoine : rouvrir un sujet récurrent pour les réunions CD, après la PyConFR
   * Antoine : s'occuper de contacter kiloutou pour la location de 2 percolateurs 7L pour le jeudi-vendredi → annulé
   * Antoine : rappeler les foodtrucks pour donner l'évolution du nombre de participant·e·s → fait
   * Lucie : contacter la responsable de l'atelier pour les enfants pour annoncer que c'est trop tard




## Ordre du jour

   * Composition du Comité Directeur suite à la l'AGO
       * Thomas Bouchet, élu en 2020
       * Pierre Bousquié, élu en 2020
       * Antoine Rozo, élu en 2020
       * Laurine Leulliette, élue en 2022
       * Lucie Anglade, élue en 2023
       * Marc Debureaux, élu en 2023
       * Agnès Haasser, élue en 2024
       * Julie Rymer, élue en 2024
       * Paul Guichon, élu en 2024
   * Élection du nouveau bureau
       * Lucie a été désignée présidente lors de l'AG
       * Pierre est nommé trésorier
       * Antoine est nommé secrétaire
       * Marc est nommé vice-président
       * Thomas est nommé vice-trésorier
       * Laurine est nommée vice-secrétaire
   * Paiement à Annabelle pour le logo, les affiches et la charte graphique
       * Annabelle ne nous a pas communiqué de montant pour sa prestation graphique
       * Suite à différents devis effectués auprès d'autres graphistes pour avoir une idée, il est décidé que le montant versé à Annabelle ne serait pas inférieur à 1500€
   * Choix du créneau / de la fréquence pour les réunions CD
       * Il est décidé que les réunions mensuelles auront lieu à 20h30 le 3ème mardi de chaque mois


## Actions

   * Lucie : donner les droits helloasso à Antoine pour accéder à la liste des adhérent·e·s
   * Marc : communiquer à Julien la liste des membres du CD et du bureau




## Actions reportées indéfiniment

   * Thomas : se renseigner pour savoir si l’association peut avoir le statut d’intérêt général
   * debnet : Prendre contact avec Gandi pour l'interview et obtenir les questions → voir une occasion pour aller sur Paris et coupler avec Gandi
   * Pierre : se renseigner auprès d'une maison d'asso sur les aspects juridiques concernant la réception de donation / rémunération d'intervenant·e·s -> Relancé le 16/10/23, pas de réponse -> Toujours pas de réponse (au 20/02/24)


Prochaine réunion le 19 novembre à 20h30



**Fin : 12h50**
