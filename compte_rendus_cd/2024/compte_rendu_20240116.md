
# Compte rendu point CD AFPy



- [https://git.afpy.org/AFPy/gestion/src/branch/master/compte\_rendus\_cd/2023]([]https://git.afpy.org/AFPy/gestion/src/branch/master/compte\_rendus\_cd/2023[])

- [https://bbb.afpy.org/b/jul-cb2-1bd-ost](https://bbb.afpy.org/b/jul-cb2-1bd-ost)



## 16 janvier 2024



**Début prévu : 19h00**



Présents :

- [x] Lucie Anglade

- [x] Marc Debureaux

- [x] Thomas Bouchet

- [ ] Bruno Bonfils

- [ ] Pierre Bousquié

- [x] Antoine Rozo

- [x] Laurine Leulliette

- [x] Jean Lapostolle

- [ ] Nicolas Ledez



- [x] Hervé Sousset

- [x] Jeff D

- [ ] Julien Palard



## Informations



## Actions prévues lors de la réunion précédente



   * debnet : Prendre contact avec Gandi pour l'interview et obtenir les questions → voir une occasion pour aller sur Paris et coupler avec Gandi
   * Pierre : se renseigner auprès d'une maison d'asso sur les aspects juridiques concernant la réception de donation / rémunération d'intervenant·e·s -> Relancé le 16/10/23, pas de réponse -> Toujours pas de réponse (au 15/11/23)
   * Lucie \& Mindiell : Lucie repasse sur la refonte du site pour le CSS et Mindiell fera une PR après → en cours (Une PR a été faite, il semblerait que cela ait bien avancé) [https://git.afpy.org/Mindiell/refonte/pulls/1](https://git.afpy.org/Mindiell/refonte/pulls/1)
   * Lucie : Mail à Framasoft pour leur demander s'ils voudraient donner une conférence -> relancés, pas de nouvelle ([https://discuss.afpy.org/t/pyconfr-2024-framasoft/1792)](https://discuss.afpy.org/t/pyconfr-2024-framasoft/1792))
   * Marc : contacter les paniers de Léa pour évaluer les tarifs 2024 → [https://discuss.afpy.org/t/pyconfr-2024-le-petit-dejeuner/1793](https://discuss.afpy.org/t/pyconfr-2024-le-petit-dejeuner/1793) -> appel fait, il faut les recontacter par email avec les détails
   * Lucie : contacter Laura pour la convention avec l’université → attente de réponse de Laura
   * Lucie : rétro-planning avec les étapes principales de la PyConFR, ticket sur le Discuss, tout le monde participe hein !
   * Julien : Mettre en place la redirection pycon.fr vers pycon.fr/2024 → fait
   * Comm : Communiquer sur le fait qu'on commence à organiser la PyConFR 2024 (quand la redirection sera en place) → à faire en mars
   * Lucie : Ouvrir un sujet sur le discuss pour discuter de la campagne de dons framasoft et savoir si on a de quoi donner → sujet ouvert
   * Antoine : Pinger la comm (Melcore, Laurine) sur le post de Julien concernant HackInScience × Code Lutin pour communiquer dessus → ping fait, en attente
   * Lucie : Ouvrir un sujet pour sonder le CD sur le meilleur créneau pour organiser les réunions CD → fait




## Ordre du jour

   * Les Keynotes (qui on invite, des thématiques, langues…)
       * Comme en 2019 : personnes défrayées sur transport \& hébergement, inviter des femmes uniquement (+ incitation aux femmes à participer au CFP)
       * Essayer d'avoir une keynote en anglais (thème tech) et deux en français (une tech et l'autre moins — sujets sociétaux)
       * Décision des keynotes par l'AFPy
       * wiki april recensant les structures rassemblant des femmes dans l'informatique : [https://wiki.april.org/w/Structures\_rassemblant\_des\_femmes\_dans\_l%27informatique](https://wiki.april.org/w/Structures\_rassemblant\_des\_femmes\_dans\_l%27informatique)
       * [https://conference.pyladies.com/index.html](https://conference.pyladies.com/index.html) <= pas mal de femmes orientées python <3
       * [https://fosdem.org/2024/schedule/speaker/M33RFP/]([]https://fosdem.org/2024/schedule/speaker/M33RFP/[])
       * Prévoir une liste d'oratrices possibles sur un ticket discuss pour ensuite lancer les invitations
   * A-t-on déjà des sponsors ?
   * Le graphisme → demander à Marie-Ange ?
   * Anniversaire AFPy (20 ans en décembre 2024) → prévoir un gâteau pour la PyCon ? (voir avec les paniers de Léa si ce serait possible d'en avoir un pour le samedi après-midi)
       * Un bretzel géant ? :D
       * Prévoir une annonce / rétrospective pendant la keynote d'introduction ou une conf dédiée
       * des extraits d'anciennes conférences ?
   * Meetup en mixité choisie à Lyon, 2e édition
       * Peut-être voir avec la place des grenouilles pour faire de la comm ?
   * Adhésions 2024
       * Mail à envoyer pour l'appel à cotisations
       * communication réseaux à faire également
   * Outil visio pour les prochaines réu (proposition de retourner sur Framatalk)
       * framatalk n'est plus disponible publiquement
       * [https://meet.jit.si/](https://meet.jit.si/)
       * bbb octopuce potentiellement ?
       * Liste de C.H.A.T.O.N.S. proposant des services de visioconférence : [https://www.chatons.org/search/by-service?service\_type\_target\_id=117\&field\_alternatives\_aux\_services\_target\_id=All\&field\_software\_target\_id=All\&field\_is\_shared\_value=All\&title=](https://www.chatons.org/search/by-service?service\_type\_target\_id=117\&field\_alternatives\_aux\_services\_target\_id=All\&field\_software\_target\_id=All\&field\_is\_shared\_value=All\&title=)
       * ticket de discussion : [https://discuss.afpy.org/t/jitsi-ou-big-blue-button/](https://discuss.afpy.org/t/jitsi-ou-big-blue-button/)
   * Prochaine AGO : ok d'attendre la PyCon ?
       * Oui, doit avoir lieu pendant l'année civile, ajouter point de rappel d'envoyer les convocations en septembre dans le rétroplanning
   * Reste-t-on sur twitter ?
       * Ça reste un canal utilisé
   * Surprise or not surprise ?
       * not surprise :'(




## Actions

   * Lucie : ouvrir ticket discuss pour les keynotes
   * Antoine : ping sujet des sponsors pour savoir où ça en est
   * Lucie : contacter Marie-Ange
   * Lucie : envoyer le mail pour les adhésions
   * Melcore : post RS pour les adhésions (après le mail)
   * Antoine : mettre à jour l'événement discuss des réunions CD pour déplacer au 3ème mardi du mois 19h30
   * Thomas : valider la PR pour le site




## Actions reportées indéfiniment

   * Thomas : se renseigner pour savoir si l’association peut avoir le statut d’intérêt général


Prochaine réunion le 20 février à 19h30



**Fin : 19h54**
