
# Compte rendu point CD AFPy



- [https://git.afpy.org/AFPy/gestion/src/branch/master/compte\_rendus\_cd/2024]([]https://git.afpy.org/AFPy/gestion/src/branch/master/compte\_rendus\_cd/202[]4)

- [https://meet.evolix.org/afpy-cd](https://meet.evolix.org/afpy-cd)



## 21 mai 2024



**Début prévu : 19h30**



Présents :

- [x] Lucie Anglade

- [x] Marc Debureaux

- [ ] Thomas Bouchet

- [ ] Bruno Bonfils

- [x] Pierre Bousquié

- [x] Antoine Rozo

- [x] Laurine Leulliette

- [ ] Jean Lapostolle

- [ ] Nicolas Ledez



- [x] Hervé Sousset

- [ ] Jeff D

- [ ] Julien Palard



## Informations



## Actions prévues lors de la réunion précédente

   * Marc : contacter les paniers de Léa pour évaluer les tarifs 2024 → [https://discuss.afpy.org/t/pyconfr-2024-le-petit-dejeuner/1793](https://discuss.afpy.org/t/pyconfr-2024-le-petit-dejeuner/1793) -> il faut les recontacter par email avec les détails
   * Lucie : contacter Laura pour la convention avec l’université → attente de réponse de l'université
   * Pierre : regarder si on peut upgrade le compte Meetup pour créer plus de groupes avec un tarif association -> Not done (mais renouvellement auto)
   * Lucie : contacter la Place Des Grenouilles pour la PyConFR -> pas fait
   * Pierre : répondre à la proposition du sponsor -> fait, en attente de réponse
   * Melcore (+ Laurine ?) : les posts / articles pour la communication sur l’event -> fait, à relire avant d’envoyer
   * Marc : transmettre article en anglais contact PyConUS -> en attente
   * Melcore : créer le sujet dans le Discuss pour l’appel à volontaire -> en cours
   * Pierre : Revoir le stock de goodies/tourdecoup -> Not Done
   * Melcore : Contenu affiche
   * Hervé : se renseigner sur les prix pour une nappe pour les JRES -> entre 50 et 100€ suivant la longueur, peut-être avoir 2 nappes. Mettre logo de l'AFPy sur le dessus et le nom sur les jupes, pour pouvoir les réutiliser / accrocher au mur
   * Marc : envoyer la demande à la PSF -> en cours, remplissage du (long) formulaire
   * Lucie : transmettre lettre EPS à Marc pour PSF -> Fait
   * Lucie : ouvrir sujet pour la captation sur le Discuss -> Fait
   * Lucie : préparer une PR pour mettre en prod sur le site l’ouverture du CFP et des bourses -> Fait
   * Lucie : ajouter Hervé dans PyConFR-orga -> Fait
   * Hervé : ouvrir sujet dans Association pour location logement pendant la PyConFR
   * Antoine: contacter les foodtrucks → en cours, doit relancer et en contacter de nouveaux




## Ordre du jour

   * Les sponsors / les sous
   * Les keynotes
       * Bon pour les deux keynotes FR, encore en attente de réponse pour la keynote EN
       * On peut tenter de contacter une autre personne pour la conférence EN
   * La com
       * présentation des formats (sprint / atelier / conférence), profiter pour faire un rappel du CFP
       * annonce des keynotes validées
       * le mail aux gens qui étaient là l’an dernier -> prévu pour jeudi (annonce le CFP, les bourses, les keynotes et appel à sponsors)
       * Woman Tech Maker : invité·e·s à présenter la pyconfr à ce meetup
   * Les t-shirts
       * l’an dernier / approximation du nombre par taille
           * Taille         Nombre
           * Ajusté L         16
           * Ajusté M         22
           * Ajusté S         7
           * Droit L         28
           * Droit M         31
           * Droit S         5
           * Droit XL         31
           * Droit XXL         10 
           * manquent très grands (XXXL) et très petits (XS)
       * chercher des prestataires / devis
   * EuroPython




## Actions

   * Antoine : aider Jeff pour trouver des associations de captation vidéo
   * Lucie : contacter une nouvelle personne pour la keynote EN
   * Pierre : retrouver le nombre de t-shirts vendus par taille -> fait
   * Pierre : chercher des prestataires pour faire des devis pour les t-shirts
   * Lucie : ouvrir sujet pour les goodies




## Actions reportées indéfiniment

   * Thomas : se renseigner pour savoir si l’association peut avoir le statut d’intérêt général
   * debnet : Prendre contact avec Gandi pour l'interview et obtenir les questions → voir une occasion pour aller sur Paris et coupler avec Gandi
   * Pierre : se renseigner auprès d'une maison d'asso sur les aspects juridiques concernant la réception de donation / rémunération d'intervenant·e·s -> Relancé le 16/10/23, pas de réponse -> Toujours pas de réponse (au 20/02/24)


Prochaine réunion le 18 juin à 19h30



**Fin : 20h22**
