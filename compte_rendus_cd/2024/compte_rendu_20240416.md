
# Compte rendu point CD AFPy



- [https://git.afpy.org/AFPy/gestion/src/branch/master/compte\_rendus\_cd/2024]([]https://git.afpy.org/AFPy/gestion/src/branch/master/compte\_rendus\_cd/202[]4)

- [https://meet.evolix.org/afpy-cd](https://meet.evolix.org/afpy-cd)



## 16 avril 2024



**Début prévu : 19h30**



Présents :

- [x] Lucie Anglade

- [x] Marc Debureaux

- [ ] Thomas Bouchet

- [ ] Bruno Bonfils

- [x] Pierre Bousquié

- [ ] Antoine Rozo

- [x] Laurine Leulliette

- [x] Jean Lapostolle

- [ ] Nicolas Ledez



- [x] Hervé Sousset

- [x] Jeff D

- [ ] Julien Palard



## Informations



## Actions prévues lors de la réunion précédente

   * Lucie : Mail à Framasoft pour leur demander s'ils voudraient donner une conférence -> [https://discuss.afpy.org/t/pyconfr-2024-framasoft/1792](https://discuss.afpy.org/t/pyconfr-2024-framasoft/1792)
   * Marc : contacter les paniers de Léa pour évaluer les tarifs 2024 → [https://discuss.afpy.org/t/pyconfr-2024-le-petit-dejeuner/1793](https://discuss.afpy.org/t/pyconfr-2024-le-petit-dejeuner/1793) -> il faut les recontacter par email avec les détails
   * Lucie : contacter Laura pour la convention avec l’université → attente de réponse de Laura
   * Pierre : regarder si on peut upgrade le compte Meetup pour créer plus de groupes avec un tarif association
   * Lucie : contacter les personnes pour les keynotes -> Houley OK, Carolyn en attente de réponse
   * Hervé : relancer Nicolas pour le JRES -> done
   * Lucie : contacter la PDG pour la PyConFR -> pas fait
   * Pierre : répondre à la proposition du sponsor -> pas fait
   * Melcore (+ Laurine ?) : les posts / articles pour la communication sur l’event -> fait, à relire avant d’envoyer
   * Marc : transmettre article en anglais contact PyConUS -> en attente
   * Marc : mettre en prod PretalX (voir avec Julien) -> fait \o/
   * Lucie : vérifier les textes sur le CFP -> fait \o/
   * Lucie : ouvrir un sujet sur le Discuss pour le modèle de lettre d’invitation bien blindax (dans PyConFR-orga) -> fait, Laurine et Antoine sur le coup
   * Pierre : Mettre en ligne les sponsors/PRs -> fait
   * Jeff : faire les demandes de devis pour la soirée du samedi -> Le Tigre et les Haras contactés.
       * Les Haras répondus très vite, il faudrait privatiser l’ensemble si on veut plus de 120 personnes mais ça coûte reuch de ouf car chef étoilé (du coup pas le Haras).
       * Le Tigre sûrement moyen de réserver "le grand Tigre" (~150/200 personnes). Moyen le végé et ils s’y connaissent pas de ouf en végan. Possible de réserver juste le lieu et de cumuler avec un traiteur externe / mix eux et traiteur externe végan.
   * Melcore : créer le sujet dans le Discuss pour l’appel à volontaire -> en cours




## Ordre du jour

   * Des stickers AFPy et une affiche (A4 ?) pour les JRES ?
       * Réfléchir au contenu une affiche
       * Nappe ? à voir suivant les prix
       * Stickers : pourquoi pas
   * Points sponsors
       * 5 factures envoyées, 3 encaissées
       * EPS et DSF demandes envoyées
   * Prestataire captation
       * Idéalement voir avec des gens de l’université
   * Modalités bourses
       * Même montant que les années précédentes
       * Voir le budget total un peu plus tard (juillet)




## Actions

   * Pierre : Revoir le stock de goodies/tourdecoup
   * Pierre : regarder si on peut upgrade le compte Meetup pour créer plus de groupes avec un tarif association
   * Pierre : répondre à la proposition du sponsor
   * Melcore : Contenu affiche
   * Hervé : se renseigner sur les prix pour une nappe
   * Marc : envoyer la demande à la PSF
   * Lucie : transmettre lettre EPS à Marc pour PSF
   * Lucie : ouvrir sujet pour la captation sur le Discuss
   * Lucie : préparer une PR pour mettre en prod sur le site l’ouverture du CFP et des bourses
   * Lucie : ajouter Hervé dans PyConFR-orga
   * Hervé : ouvrir sujet dans Association pour location logement pendant la PyConFR




## Actions reportées indéfiniment

   * Thomas : se renseigner pour savoir si l’association peut avoir le statut d’intérêt général
   * debnet : Prendre contact avec Gandi pour l'interview et obtenir les questions → voir une occasion pour aller sur Paris et coupler avec Gandi
   * Pierre : se renseigner auprès d'une maison d'asso sur les aspects juridiques concernant la réception de donation / rémunération d'intervenant·e·s -> Relancé le 16/10/23, pas de réponse -> Toujours pas de réponse (au 20/02/24)


Prochaine réunion le 21 mai à 19h30



**Fin : 20h21**
