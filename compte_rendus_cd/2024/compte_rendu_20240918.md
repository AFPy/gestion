
# Compte rendu point CD AFPy



- [https://git.afpy.org/AFPy/gestion/src/branch/master/compte\_rendus\_cd/2024]([]https://git.afpy.org/AFPy/gestion/src/branch/master/compte\_rendus\_cd/202[]4)

- [https://meet.evolix.org/afpy-cd](https://meet.evolix.org/afpy-cd)



## 18 septembre 2024



**Début prévu : 19h30**



Présents :

- [x] Lucie Anglade

- [x] Marc Debureaux

- [x] Thomas Bouchet

- [ ] Bruno Bonfils

- [x] Pierre Bousquié

- [x] Antoine Rozo

- [x] Laurine Leulliette

- [x] Jean Lapostolle

- [ ] Nicolas Ledez



- [x] Hervé Sousset

- [x] Jeff D

- [ ] Julien Palard



## Informations



## Actions prévues lors de la réunion précédente

   * Pierre : regarder si on peut upgrade le compte Meetup pour créer plus de groupes avec un tarif association -> Mail en cours (au 16/07/24)
   * Melcore : créer le sujet dans le Discuss pour l’appel à volontaire -> en cours, sera fait ~~rapidement~~
   * Pierre : Revoir le stock de goodies/tourdecoup/anciens t-shirts -> fait, pas besoin de recommander de nouveaux tours de cou
   * Melcore : Contenu affiche -> todo (pas prio) (pour les JRES)
   * Pierre : chercher des prestataires pour faire des devis pour les t-shirts -> Plusieurs mais un peu tôt pour fin septembre.  Prix entre 10-15€TTC
   * Marc : la pub sur ReadTheDocs [https://docs.readthedocs.io/en/stable/advertising/ethical-advertising.html#community-ads](https://docs.readthedocs.io/en/stable/advertising/ethical-advertising.html#community-ads) -> faire pub pour l'événement (trop tard pour le cfp)
   * Marc : demander à ta femme pour du sponsoring :D -> en attente de réponse (relancé), conditionné à leur budget
   * Marc: Faire une interface propre en amont pour la gestion du pointage à l'entrée de la PyConFR
   * Lucie: Redemander à l'université si on peut condamner une des deux entrées → en attente de validation du plan par l'université
   * Laurine / Melcore : Comm pour relancer les sponsors d'ici fin ~~août~~ septembre -> à faire
   * Melcore : Écrire une page sur le site de l'AFPy pour présenter le CD (pas prio)
   * Antoine: relancer DALI début septembre -> fait (enfin, ils sont revenus d'eux-mêmes)




## Ordre du jour

   * Peu d'inscriptions actuellement → relancer la communications pour indiquer que l'inscription est obligatoire ?
       * Et s'assurer que les speakers ont fait leur inscription
   * Droits d’utilisation des enregistrements des conférences
       * Un speaker qui veut réutiliser la vidéo pour la rediffuser chez lui
       * Ok pour la rediffusion par les speakers, au cas par cas pour la rediffusion ailleurs (en fonction de la licence de la présentation)
   * Lancer les convocations pour l'assemblée générale ordinaire
   * Pretalx c’est clairement pas fait pour les appels à volontaires. Un sujet sur le discuss sur lequel les gens se notent ?
   * Budget
       * à peu près ok parce qu'on a de la trésorerie en avance, mais déficitaires pour l'événement
       * ce serait bien d'entrer au moins un sponsor or, et quelques petits de dernière minute
       * Budget goodies estimé à la hausse, bourses à discuter
   * Location de voiture sur place
       * Monospace pour pouvoir au moins aller chercher / ramener Raffut (2-3 personnes) et leur matériel à la gare
       * Passer par une agence de location classique ?
   * Tour du rétroplanning




## Actions

   * Lucie : Annonce sur LinuxFR pour le planning et le rappel des inscriptions
   * Lucie : Ouvrir un sujet sur le discuss pour l'AGO et le renouvellement du CD (démissionnaires ?)
   * Marc : envoyer la facture pour sponsoring PSF
   * Marc \& Antoine : trouver un bar pour une vingtaine de personnes pour accueillir les gens le vendredi soir de la PyConFR
   * Lucie / Guillaume : faire un PDF du planning
   * Lucie : souligner les liens sur le site PyConFR






## Actions reportées indéfiniment

   * Thomas : se renseigner pour savoir si l’association peut avoir le statut d’intérêt général
   * debnet : Prendre contact avec Gandi pour l'interview et obtenir les questions → voir une occasion pour aller sur Paris et coupler avec Gandi
   * Pierre : se renseigner auprès d'une maison d'asso sur les aspects juridiques concernant la réception de donation / rémunération d'intervenant·e·s -> Relancé le 16/10/23, pas de réponse -> Toujours pas de réponse (au 20/02/24)


Prochaine réunion le 8 octobre à 19h30



**Fin : 20h34**
