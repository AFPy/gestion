# Compte rendu point CD AFPy



- [https://git.afpy.org/AFPy/gestion/src/branch/master/compte\_rendus\_cd/2024]([]https://git.afpy.org/AFPy/gestion/src/branch/master/compte\_rendus\_cd/202[]4)

- [https://meet.evolix.org/afpy-cd](https://meet.evolix.org/afpy-cd)



## 20 février 2024



**Début prévu : 19h30**



Présents :

- [x] Lucie Anglade

- [x] Marc Debureaux

- [ ] Thomas Bouchet

- [ ] Bruno Bonfils

- [X] Pierre Bousquié

- [x] Antoine Rozo

- [x] Laurine Leulliette

- [ ] Jean Lapostolle

- [ ] Nicolas Ledez



- [x] Hervé Sousset

- [x] Jeff D

- [ ] Julien Palard



## Informations



## Actions prévues lors de la réunion précédente



   * debnet : Prendre contact avec Gandi pour l'interview et obtenir les questions → voir une occasion pour aller sur Paris et coupler avec Gandi
   * Pierre : se renseigner auprès d'une maison d'asso sur les aspects juridiques concernant la réception de donation / rémunération d'intervenant·e·s -> Relancé le 16/10/23, pas de réponse -> Toujours pas de réponse (au 20/02/24)
   * Lucie : Mail à Framasoft pour leur demander s'ils voudraient donner une conférence -> relancés, pas de nouvelle ([https://discuss.afpy.org/t/pyconfr-2024-framasoft/1792)](https://discuss.afpy.org/t/pyconfr-2024-framasoft/1792))
   * Marc : contacter les paniers de Léa pour évaluer les tarifs 2024 → [https://discuss.afpy.org/t/pyconfr-2024-le-petit-dejeuner/1793](https://discuss.afpy.org/t/pyconfr-2024-le-petit-dejeuner/1793) -> appel fait, il faut les recontacter par email avec les détails
   * Lucie : contacter Laura pour la convention avec l’université → attente de réponse de Laura
   * Antoine : Pinger la comm (Melcore, Laurine) sur le post de Julien concernant HackInScience × Code Lutin pour communiquer dessus → ping fait, en attente
   * Lucie : ouvrir ticket discuss pour les keynotes → fait
   * Antoine : ping sujet des sponsors pour savoir où ça en est → fait
   * Lucie : contacter Marie-Ange → fait
   * Lucie : envoyer le mail pour les adhésions → fait
   * Melcore : post RS pour les adhésions (après le mail) → fait
   * Antoine : mettre à jour l'événement discuss des réunions CD pour déplacer au 3ème mardi du mois 19h30 → fait
   * Thomas : valider la PR pour la refonte CSS du site




## Ordre du jour

   * News à propos du lieu
       * Mail envoyé au directeur sans trop dé réponse au départ
       * Relance du directeur pour savoir si les salles nous intéressaient toujours
       * Laura doit organiser un point avec Jeff et le directeur pour mettre en place la convention, toujours en attente
       * Il devrait y avoir ~1 mois de retard sur le rétroplanning prévu
   * News à propos du graphisme
       * Marie-Ange ne peut pas cette année
       * Discussions avec Kozea pour savoir s'ils seraient partants
       * Sinon Annabelle est partante
   * Payer la personne qui fait le graphisme
       * → Oui
   * Sélection des keynotes
       * choisir les speakeuses
       * quel montant max on peut financer pour les invitations ? >> comme pour les bourses (max 400€)
   * PonyConf
       * avoir un champ obligatoire pour que les personnes indiquent la langue de leur atelier / sprint / conférence
       * ne pas perdre l’information de la note d’une proposition une fois qu’elle a été acceptée ou refusée
       * dans la partie admin, pouvoir indiquer si une conférence sera accessible LSF (on sait jamais si on a un sponsoring pour ça)
   * Communication sur Libre à vous pour la pycon ? >> Oui, quand on aura lancé la com
   * Relance des meetups à Montpellier >> Voir les prix pour l’abonnement meetup.com
   * Contact des sponsors → on a un message type à envoyer ou c'est différent pour chaque contact ? → oui, dispo sur discuss [https://discuss.afpy.org/t/template-demail/1900](https://discuss.afpy.org/t/template-demail/1900)




## Actions

   * Marc : Fouetter Mindiell sur la place publique pour la validation de la PR CSS
   * Lucie : Demander à Annabelle à combien (€€€) elle évalue la création d'un logo
   * Lucie : Créer un sondage interne au CD pour le choix des keynotes
   * Marc : features PonyConf
   * Lucie : Créer le ticket features PonyConf sur le discuss
   * Lucie : discuter outils pour les CfP (pas forcément pour cette année)
   * Lucie : contacter Libre à vous! pour la PyConFR
   * Pierre : regarder si on peut upgrade le compte Meetup pour créer plus de groupes
   * Lucie : ping Nicolas pour le JRES, deadline mai




## Actions reportées indéfiniment

   * Thomas : se renseigner pour savoir si l’association peut avoir le statut d’intérêt général


Prochaine réunion le 19 mars à 19h30



**Fin : 20h26**
