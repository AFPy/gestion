# Compte rendu point CD AFPy



- [https://git.afpy.org/AFPy/gestion/src/branch/master/compte\_rendus\_cd/2024]([]https://git.afpy.org/AFPy/gestion/src/branch/master/compte\_rendus\_cd/202[]4)

- [https://meet.evolix.org/afpy-cd](https://meet.evolix.org/afpy-cd)



## 16 juillet 2024



**Début prévu : 19h30**



Présents :

- [x] Lucie Anglade

- [x] Marc Debureaux

- [ ] Thomas Bouchet

- [ ] Bruno Bonfils

- [x] Pierre Bousquié

- [x] Antoine Rozo

- [x] Laurine Leulliette

- [x] Jean Lapostolle

- [ ] Nicolas Ledez



- [x] Hervé Sousset

- [x] Jeff D

- [ ] Julien Palard



## Informations



## Actions prévues lors de la réunion précédente

   * Pierre : regarder si on peut upgrade le compte Meetup pour créer plus de groupes avec un tarif association -> Mail en cours (au 16/07/24)
   * Melcore : créer le sujet dans le Discuss pour l’appel à volontaire -> en cours, sera fait rapidement
   * Pierre : Revoir le stock de goodies/tourdecoup/anciens t-shirts -> Retrouvé, pas fait les comptes
   * Melcore : Contenu affiche -> todo (pour les JRES)
   * Hervé : ouvrir sujet dans Association pour location logement pendant la PyConFR -> un peu plus proche de la date de la PyConFR, à faire courant août
   * Antoine: contacter les foodtrucks → en cours, doit relancer et en contacter de nouveaux (toujours)
   * Antoine : aider Jeff pour trouver des associations de captation vidéo -> redirigé vers un prestataire, pas de nouvelles
   * Pierre : chercher des prestataires pour faire des devis pour les t-shirts -> Plusieurs mais un peu tôt pour fin octobre.  Ou fait-on livrer? Prix entre 10-15€TTC
   * Marc : rework le message de com pour le Discord Python pour annoncer la PyConFR et le CFP -> fait
   * Lucie : demander à l’université si on peut faire livrer les t-shirts là-bas, sinon chez Jeff ou si on trouve une personne de l’université -> ok
   * Marc : la pub sur ReadTheDocs [https://docs.readthedocs.io/en/stable/advertising/ethical-advertising.html#community-ads](https://docs.readthedocs.io/en/stable/advertising/ethical-advertising.html#community-ads) -> faire pub pour l'événement (trop tard pour le cfp)
   * Laurine : com pour la garderie / espace enfants -> fait
   * Lucie : rajouter sur HelloAsso / le site que l’on rembourse pas les trucs payés après la conférence -> pas fait
   * Jeff : Contacter Le Tigre et The Social Bar pour la soirée du samedi -> fait, prestation Le Tigre ok (avec formule vegan), The Social Bar compliqué (au moins 150 personnes) -> Partons sur le tigre (+ souple)
   * Laurine : posts pour le CFP (rappel et présentation des différents formats) -> rappel fait
   * Marc : demander à ta femme pour du sponsoring :D -> en attente de réponse, conditionné à leur budget




## Ordre du jour

   * L’université (sécu + évènement en parallèle)
       * 3 agents de sécurité (1 pour chaque entrée + 1 mobile) nécessaires
       * Devis pour le vendredi/samedi/dimanche (jeudi pas nécessaire car l'université est ouverte) de 4800€ TTC
       * Voir pour condamner une entrée ?
       * Fournir un plan pour savoir où seront installés les différents postes (stands, accueil, petit déjeuner, etc.)
       * Événement en parallèle le jeudi matin (jusque 14h) dans l'université, 2 salles que l'on a pour les sprints ne seront accessibles qu'à partir du jeudi après-midi
   * Foodtrucks (autorisation)
       * Demander une autorisation à la DALI pour l'installation des foodtrucks (on a déjà l'AOT signée par le directeur)
       * Pas d'installation d'électricité sur place
   * Des nouveautés sur les sponsors ?
       * Qulques entrées récentes, toujours 3 sponsors en discussion
       * 22000€ sur le compte, devis de 10000€ pour les repas signé, devis de sécu 4800€ bientôt signé
       * pas encore de sponsor or
   * Où en est-on du CFP ?
       * Actuellement 128 propositions (75 anglais + 53 français) dont 2 retirées + 4 brouillons
       * 87 personnes différentes à proposer une conf
       * Seulement 3 propositions de sprints et 12 ateliers (majoritairement en anglais)
       * 9 salles pour les sprints (7 salles le jeudi matin) le jeudi-vendredi
       * 7 salles pour les ateliers, 2 amphis et 2 salles pour les confs le samedi-dimanche
       * 1 salle ou 2 pour l'espace enfants. Nombre d'enfants à donner 15j avant, peut-être prévoir une billetterie dédiée (avec 14 places plutôt que 18 pour garder un peu de marge ?). Donner des infos sur l'espace enfant dans les mails d'approbation des conférences.
   * Informations sur le CD
       * 0 information à l'extérieur sur le rôle de chacun au sein du CD, sur le rôle du CD dans l'association pour motiver des gens à le rejoindre plutôt que recruter à la dernière minute pendant les AGO
       * Faire une page sur le site pour présenter tout ça




## Actions

   * Marc: Faire une interface propre en amont pour la gestion du pointage à l'entrée de la PyConFR
   * Lucie: Redemander à l'université si on peut condamner une des deux entrées
   * Antoine: Faire la demande à la DALI Université de Strasbourg pour les foodtrucks
   * Laurine / Melcore : Comm pour relancer les sponsors d'ici fin août
   * Laurine / Melcore : Comm de dernière minute 48h avant la fin du CFP (vendredi)
   * Hervé : Ouvrir un sujet dans pyconfr-orga concernant les nappes AFPy pour divers événements
   * Melcore : Écrire une page sur le site de l'AFPy pour présenter le CD




## Actions reportées indéfiniment

   * Thomas : se renseigner pour savoir si l’association peut avoir le statut d’intérêt général
   * debnet : Prendre contact avec Gandi pour l'interview et obtenir les questions → voir une occasion pour aller sur Paris et coupler avec Gandi
   * Pierre : se renseigner auprès d'une maison d'asso sur les aspects juridiques concernant la réception de donation / rémunération d'intervenant·e·s -> Relancé le 16/10/23, pas de réponse -> Toujours pas de réponse (au 20/02/24)


Prochaine réunion le 20 août à 19h30



**Fin : 20h19**
