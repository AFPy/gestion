# Compte rendu point CD AFPy



- [https://git.afpy.org/AFPy/gestion/src/branch/master/compte\_rendus\_cd/2024]([]https://git.afpy.org/AFPy/gestion/src/branch/master/compte\_rendus\_cd/202[]4)

- [https://meet.evolix.org/afpy-cd](https://meet.evolix.org/afpy-cd)



## 17 décembre 2024



**Début prévu : 20h30**



Présents :

- [X] Lucie Anglade

- [X] Marc Debureaux

- [x] Thomas Bouchet

- [x] Pierre Bousquié

- [ ] Antoine Rozo [absent prévenu]

- [X] Laurine Leulliette

- [x] Agnès Haasser

- [X] Julie Rymer

- [x] Paul Guichon



- [X] Julien Palard

- [ ] Hervé Sousset





## Informations



## Actions prévues lors de la réunion précédente

   * Pierre : regarder si on peut upgrade le compte Meetup pour créer plus de groupes avec un tarif association -> Mail en cours (au 16/07/24)
   * Melcore : Écrire une page sur le site de l'AFPy pour présenter le CD (pas prio) -> en cours
   * Antoine : Reprendre l'action de Melcore sur la page de présentation du CD → à faire (pas fait)
   * Marc : Ajouter le pointage des adhérent·e·s de l'asso (pour l'AGO) dans l'outil de pointage PyConFR
   * Lucie : Demander à Pierre quelles finances on a pour les dons à des assos -> trésorerie présente, demander à Framasoft pour le don (et le montant)
   * Lucie : répondre à Strype → fait (19/11)
   * Antoine : Redemander les adresses postales + professions des personnes du CD pour la déclaration en préfecture → à faire (pas fait)




## Ordre du jour

   * Migration de gitea à forgejo ?
       * Gitea a bougé récemment son logo et sa marque vers une structure commerciale. C'est géré plutôt côté Asie
       * Forgejo est plutôt basé en Europe et a l'air d'être un fork intéressant mais qui diverge actuellement, donc si on veut bouger, il ne faut pas forcément trop traîner (actuellement, ça reste rapide de basculer)
       * D'autres signaux positifs pour Forgejo : Codeberg, NLNET, Fedora, Easter-eggs
       * Interrogations pour la prononciation du nom. Apparemment "forgéyo".
       * Pas d'autres fork connus à côté de Forgejo
       * Sondage plus tard posté sur le discourse
   * Stickers Python sur demande et polos Python ([https://discuss.afpy.org/t/sur-bordeaux-meetup-le-11-decembre/2366/7)](https://discuss.afpy.org/t/sur-bordeaux-meetup-le-11-decembre/2366/7))
       * Polo → boîte située en Espagne sur le sujet
       * Stickers → via StickerMule ? plutôt StickerApp
       * Il existe des sites de e-commerce pour gérer des commandes automatiques sur lesquelles on toucherait des commissions : [https://enventelibre.org](https://enventelibre.org) pourrait peut-être servir ?
       * Question sur les droits sur le logo de l'AFPy → accord donné par la PSF (sous couvert de conditions indiqués sur leur site)
   * Relecture du PV de l'assemblée générale : est-ce qu'on est bon pour envoyer à la préfecture ?
       * Publication sur Gitea du récépissé de la Préfecture des adresses postales des membres du Bureau → problème de vie privée
       * Solutions ?
           * [ ] Ne pas le publier sur Gitea
           * [x] Chiffrer le document \& rendre accessible via le pass avec (age)[[https://github.com/FiloSottile/age](https://github.com/FiloSottile/age)] par exemple, en stockant la clé de déchiffrement dans [pass]([https://git.afpy.org/AFPy/pass)](https://git.afpy.org/AFPy/pass)).
           * [ ] Censurer le document avant publication
   * Babychou
       * Relance effectuée pour une facture avec le même numéro que la facture initiale mais au montant différent
       * Différence avec le devis signé (tarifs différents avec la 2nde facture)
       * Relance d'abord avec Babychou de la situation, à voir les actions par la suite
       * Demander une facture de régularisation par rapport au devis
   * On a pas la bonne adresse sur le siren/... [https://www.journal-officiel.gouv.fr/pages/associations-detail-annonce/?q.id=id:201700461213](https://www.journal-officiel.gouv.fr/pages/associations-detail-annonce/?q.id=id:201700461213)
       * Double typo "Profess**ue**r" et "Zimmermann**n**"
   * Point Tréso: Solde instantané :
       *  5 631,86 EUR  sur socgen 
       * 16 652,22 € EUR* sur paypal
           * 3 751,92 € EUR
           * 14 000,00 $ USD
   * Don pour Framasoft
       * Sondage effectué : 1000 euros (6 votes sur 8)


## Actions

   * Pierre : regarder si on peut upgrade le compte Meetup pour créer plus de groupes avec un tarif association -> Mail en cours (au 16/07/24)
   * Antoine : Écrire une page sur le site de l'AFPy pour présenter le CD
   * Antoine : Redemander les adresses postales + professions des personnes du CD pour la déclaration en préfecture
   * Marc : Ajouter le pointage des adhérent·e·s de l'asso (pour l'AGO) dans l'outil de pointage PyConFR
   * Pierre : Payer la bonne facture du Tigre (-2116,50€)
   * Pierre : Payer la dernière bourse (-400€)
   * Pierre : Encaisser la caisse (+892.26€)
   * Pierre : Répartir l’argent entre paypal et socgen.
   * Pierre : ... payer babychou?... enfin après résolution du tarif... (-300?€)
   * Pierre : Payer Raffut ... en attente de facture. (env -4000€)
   * Pierre : Faire corriger la faute d’orthographe sur le journal-officiel. 
   * Pierre : Publier les photos sur [https://photos.afpy.org/](https://photos.afpy.org/)
   * Lucie : regarder les tarifs pour les stickers sur StickerApp
   * Agnès / Mindiell : regarder pour les polos sur [https://enventelibre.org](https://enventelibre.org) (ou autre site)
   * Mindiell : rappeler Babychou (pour pas que Lucie s'énerve)
   * Pierre : Faire un don de 1000 euros à Framasoft (don unique 1 fois hein!)




## Actions reportées indéfiniment

   * Thomas : se renseigner pour savoir si l’association peut avoir le statut d’intérêt général
   * debnet : Prendre contact avec Gandi pour l'interview et obtenir les questions → voir une occasion pour aller sur Paris et coupler avec Gandi
   * Pierre : se renseigner auprès d'une maison d'asso sur les aspects juridiques concernant la réception de donation / rémunération d'intervenant·e·s -> Relancé le 16/10/23, pas de réponse -> Toujours pas de réponse (au 20/02/24)


Prochaine réunion le 21 janvier à 20h30



**Fin : 21h26**
