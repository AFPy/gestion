
# Compte rendu point CD AFPy



- [https://git.afpy.org/AFPy/gestion/src/branch/master/compte\_rendus\_cd/2024]([]https://git.afpy.org/AFPy/gestion/src/branch/master/compte\_rendus\_cd/202[]4)

- [https://meet.evolix.org/afpy-cd](https://meet.evolix.org/afpy-cd)



## 8 octobre 2024



**Début prévu : 19h30**



Présents :

- [x] Lucie Anglade

- [X] Marc Debureaux

- [X] Thomas Bouchet

- [ ] Bruno Bonfils

- [x] Pierre Bousquié

- [x] Antoine Rozo

- [x] Laurine Leulliette

- [x] Jean Lapostolle

- [ ] Nicolas Ledez



- [x] Hervé Sousset

- [x] Jeff D

- [ ] Julien Palard

- [x] Agnès Haasser



## Informations



## Actions prévues lors de la réunion précédente

   * Pierre : regarder si on peut upgrade le compte Meetup pour créer plus de groupes avec un tarif association -> Mail en cours (au 16/07/24)
   * Melcore/Laurine : créer le sujet dans le Discuss pour l’appel à volontaire -> en cours, sera fait ~~rapidement~~
   * Melcore : Contenu affiche -> todo (pas prio) (pour les JRES)
   * Pierre : chercher des prestataires pour faire des devis pour les t-shirts -> Plusieurs mais un peu tôt pour fin septembre.  Prix entre 10-15€TTC -> commande confirmée (impression double-pression)
   * Marc : la pub sur ReadTheDocs [https://docs.readthedocs.io/en/stable/advertising/ethical-advertising.html#community-ads](https://docs.readthedocs.io/en/stable/advertising/ethical-advertising.html#community-ads) -> faire pub pour l'événement (trop tard pour le cfp), pas fait sur RTD mais fait sur le Discord officiel
   * Marc : demander à ta femme pour du sponsoring :D -> en attente de réponse (relancé), conditionné à leur budget (toujours pas de réponse à ce jour)
   * Marc: Faire une interface propre en amont pour la gestion du pointage à l'entrée de la PyConFR -> en cours
   * Lucie: Redemander à l'université si on peut condamner une des deux entrées → en attente de validation du plan par l'université
   * Laurine / Melcore : Comm pour relancer les sponsors d'ici fin ~~août~~ septembre -> fait
   * Melcore : Écrire une page sur le site de l'AFPy pour présenter le CD (pas prio) -> en cours
   * Lucie : Annonce sur LinuxFR pour le planning et le rappel des inscriptions -> fait
   * Lucie : Ouvrir un sujet sur le discuss pour l'AGO et le renouvellement du CD (démissionnaires ?) -> fait
   * Marc : envoyer la facture pour sponsoring PSF -> fait
   * Marc \& Antoine : trouver un bar pour une vingtaine de personnes pour accueillir les gens le vendredi soir de la PyConFR
   * Lucie / Guillaume : faire un PDF du planning -> à faire
   * Lucie : souligner les liens sur le site PyConFR -> fait




## Ordre du jour

   * Journées Réseaux de l'Enseignement Supérieur  à Rennes en décembre (couvent des jacobins)
       * Salle dédiée aux exposants
       * Charte à signer, fournir un recensement définitif des personnes qui viendront représenter l'asso (2 personnes max) avec planning de présence
       * Nécessite un logo au format SVG pour le pupitre
       * Stand : prise de courant, pupitre avec logo, tableau d'affichage
       * Il faut envoyer un email pour être ajouter dans la liste de diffusion (bureau@afpy.org)
       * badges nominatifs (considérés comme congressistes)
       * deadline pour dire qui sera présent : pas de date donnée
   * Bar du vendredi soir PyConFR :
       * J'ai pensé Au Brasseur ([https://aubrasseur.fr/)](https://aubrasseur.fr/)) ou à La Lanterne ([https://www.brasserie-lalanterne.fr/)](https://www.brasserie-lalanterne.fr/))
       * Pas encore contactés, mais ils proposent des grosses réservations, à voir quelle souplesse ils ont (on n'aura jamais un nombre précis de personnes, on veut juste une table pour que des gens puissent se rajouter au fur et à mesure en gros)
       * → contacter la lanterne
   * [https://discuss.afpy.org/t/agenda-darrive-et-de-depart-des-organisateurs/2291](https://discuss.afpy.org/t/agenda-darrive-et-de-depart-des-organisateurs/2291)
   * Anniversaire de l'AFPy
   * Pouvoirs AG non reçus (un pour Antoine et un pour Pierre)
       * Ok pour accepter les procurations tant qu'une personne du CD (autre que celle qui reçoit le pouvoir) est informée
   * Sponsors ? Deadline pour être sur l’affiche
       * Deadline au 22 octobre
       * 3 sponsors en attente de logo pour l'affiche
       * Avoir tout présent le mercredi sur place pour imprimer les différentes affiches
   * Équipe diversité
       * 3 personnes sûres
       * Prévoir des affiches
       * Numéro de téléphone temporaire (proxy) ?
   * Relancer inscription des speakers ?
       * Pas nécessaire, on peut les ajouter à la dernière minute, tout comme il faudra gérer les inscriptions de dernière minute pour tout de même logger les entrées
   * Demandes de bourses en attente




## Actions

   * Hervé : savoir la deadline pour envoyer les nombres de personnes au JRES
   * Antoine : contacter la lanterne pou réserver une table pour 20 personnes le vendredi soir de la PyConFR
   * Antoine : contacter les paniers de Léa pour voir si on peut faire quelque chose pour l'anniversaire
   * Laurine : faire affiche concernant l'équipe diversité
   * Melcore : se renseigner pour le numéro proxy




## Actions reportées indéfiniment

   * Thomas : se renseigner pour savoir si l’association peut avoir le statut d’intérêt général
   * debnet : Prendre contact avec Gandi pour l'interview et obtenir les questions → voir une occasion pour aller sur Paris et coupler avec Gandi
   * Pierre : se renseigner auprès d'une maison d'asso sur les aspects juridiques concernant la réception de donation / rémunération d'intervenant·e·s -> Relancé le 16/10/23, pas de réponse -> Toujours pas de réponse (au 20/02/24)


Prochaine réunion le 22 octobre à 19h30



**Fin : 20h15**
