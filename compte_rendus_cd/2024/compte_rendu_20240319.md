
# Compte rendu point CD AFPy



- [https://git.afpy.org/AFPy/gestion/src/branch/master/compte\_rendus\_cd/2024]([]https://git.afpy.org/AFPy/gestion/src/branch/master/compte\_rendus\_cd/202[]4)

- [https://meet.evolix.org/afpy-cd](https://meet.evolix.org/afpy-cd)



## 19 mars 2024



**Début prévu : 19h30**



Présents :

- [x] Lucie Anglade

- [X] Marc Debureaux

- [ ] Thomas Bouchet

- [ ] Bruno Bonfils

- [x] Pierre Bousquié

- [ ] Antoine Rozo

- [ ] Laurine Leulliette

- [x] Jean Lapostolle

- [ ] Nicolas Ledez



- [x] Hervé Sousset

- [x] Jeff D

- [ ] Julien Palard



## Informations



## Actions prévues lors de la réunion précédente

   * Lucie : Mail à Framasoft pour leur demander s'ils voudraient donner une conférence -> relancés, pas de nouvelle ([https://discuss.afpy.org/t/pyconfr-2024-framasoft/1792)](https://discuss.afpy.org/t/pyconfr-2024-framasoft/1792))
   * Marc : contacter les paniers de Léa pour évaluer les tarifs 2024 → [https://discuss.afpy.org/t/pyconfr-2024-le-petit-dejeuner/1793](https://discuss.afpy.org/t/pyconfr-2024-le-petit-dejeuner/1793) -> appel fait, il faut les recontacter par email avec les détails
   * Lucie : contacter Laura pour la convention avec l’université → attente de réponse de Laura
   * Antoine : Pinger la comm (Melcore, Laurine) sur le post de Julien concernant HackInScience × Code Lutin pour communiquer dessus → ping fait, en attente -> fait
   * Thomas : valider la PR pour la refonte CSS du site
   * Marc : Fouetter Mindiell sur la place publique pour la validation de la PR CSS
   * Lucie : Demander à Annabelle à combien (€€€) elle évalue la création d'un logo
   * Lucie : Créer un sondage interne au CD pour le choix des keynotes -> fait
   * Marc : features PonyConf, review du PretalX -> fait, voir ordre du jour
   * Lucie : Créer le ticket features PonyConf sur le discuss -> fait
   * Lucie : discuter outils pour les CfP (pas forcément pour cette année) -> pas fait
   * Lucie : contacter Libre à vous! pour la PyConFR -> fait
   * Pierre : regarder si on peut upgrade le compte Meetup pour créer plus de groupes avec un tarif association -> pas fait
   * Lucie : ping Nicolas pour le JRES, deadline mai -> fait




## Ordre du jour

   * Un atelier Place Des Grenouilles à la PyConFR ? : les contacter pour voir un atelier / talk
   * La proposition de sponsor ([https://discuss.afpy.org/t/proposition-d-un-sponsor/2029)](https://discuss.afpy.org/t/proposition-d-un-sponsor/2029)) -> Pas de sponsoring à la lettre, égalité des sponsors, pas de soucis pour un stand si ils veulent en parler
   * Les keynotes -> sondage fini, contacter les gens
   * Init de la com’ pour annoncer les dates de l’évènement (31 octobre - 3 novembre), le lieu (Strasbourg), les date d’ouverture CFP (fin avril), on cherche des sponsors, des volontaires pour aider -> version fr / en
       * Twitter (post)
       * Mastodon (post)
       * LinkedIn (post)
       * Site PyConFR (article)
       * LinuxFR (article)
   * Commencer à recruter pour la team Diversité : déjà une personne intéressée English speaker, voir avec dernier mail sur contact@pycon.fr
   * Vérifier les textes sur le CFP (descriptions des formats…)
   * Modèle de lettre d’invitation bien blindée pour les personnes qui en ont besoin pour les visa : ouvrir un sujet sur le Discuss
   * Contacter des salles pour la soirée du samedi soir pour avoir des devis (150/200 personnes, si il y a un service traiteur avec − pas oublier pour les repas végétariens / végan −, 1 verre alcool/personne et du sans alcool)
   * Review de l'outil CfP PretalX -> c’est beau et ça a l’air bien+1+1+1
   * Comment proposer au gens de participer à l'organisation : voir en fonction de ce que les gens veulent faire (sur place, en amont etc) et faire un sujet sur le Discuss dans Association
   * 2 sponsors:  Entr’ouvert et Yaal Coop : \o/




## Actions

   * Lucie : contacter les personnes pour les keynotes
   * Hervé : relancer Nicolas pour le JRES
   * Lucie : contacter la PDG pour la PyConFR
   * Pierre : répondre à la proposition du sponsor
   * Melcore (+ Laurine ?) : les posts / articles pour la communication sur l’event
   * Marc : transmettre article en anglais contact PyConUS
   * Lucie : vérifier les textes sur le CFP
   * Lucie : ouvrir un sujet sur le Discuss pour le modèle de lettre d’invitation bien blindax (dans PyConFR-orga)
   * Pierre : Mettre en ligne les sponsors/PRs
   * Jeff : faire les demandes de devis pour la soirée du samedi
   * Marc : ouvrir le sujet dans PyConFR-Orga pour dire qu’on change de CFP
   * Marc : mettre en prod PretalX (voir avec Julien)
   * Melcore : créer le sujet dans le Discuss pour l’appel à volontaire




## Actions reportées indéfiniment

   * Thomas : se renseigner pour savoir si l’association peut avoir le statut d’intérêt général
   * debnet : Prendre contact avec Gandi pour l'interview et obtenir les questions → voir une occasion pour aller sur Paris et coupler avec Gandi
   * Pierre : se renseigner auprès d'une maison d'asso sur les aspects juridiques concernant la réception de donation / rémunération d'intervenant·e·s -> Relancé le 16/10/23, pas de réponse -> Toujours pas de réponse (au 20/02/24)


Prochaine réunion le 16 avril à 19h30



**Fin : 21h00**
