# Compte rendu point CD AFPy



- [https://git.afpy.org/AFPy/gestion/src/branch/master/compte\_rendus\_cd/2024]([]https://git.afpy.org/AFPy/gestion/src/branch/master/compte\_rendus\_cd/202[]4)

- [https://meet.evolix.org/afpy-cd](https://meet.evolix.org/afpy-cd)



## 22 octobre 2024



**Début prévu : 19h30**



Présents :

- [x] Lucie Anglade

- [x] Marc Debureaux

- [ ] Thomas Bouchet

- [ ] Bruno Bonfils

- [x] Pierre Bousquié

- [x] Antoine Rozo

- [x] Laurine Leulliette

- [ ] Jean Lapostolle

- [ ] Nicolas Ledez



- [x] Hervé Sousset

- [ ] Jeff D





## Informations



## Actions prévues lors de la réunion précédente

   * Pierre : regarder si on peut upgrade le compte Meetup pour créer plus de groupes avec un tarif association -> Mail en cours (au 16/07/24)
   * Melcore/Laurine : créer le sujet dans le Discuss pour l’appel à volontaire -> fait (3 volontaires pour l'instant)
   * Melcore : Contenu affiche -> todo (pas prio) (pour les JRES)
   * Marc : la pub sur ReadTheDocs [https://docs.readthedocs.io/en/stable/advertising/ethical-advertising.html#community-ads](https://docs.readthedocs.io/en/stable/advertising/ethical-advertising.html#community-ads) -> faire pub pour l'événement (trop tard pour le cfp), pas fait sur RTD mais fait sur le Discord officiel
   * Marc : demander à ta femme pour du sponsoring :D -> en attente de réponse (relancé), conditionné à leur budget (toujours pas de réponse à ce jour)
   * Marc: Faire une interface propre en amont pour la gestion du pointage à l'entrée de la PyConFR -> en cours
   * Lucie: Redemander à l'université si on peut condamner une des deux entrées → ok pour condamner la porte (1200€ économisés)
   * Melcore : Écrire une page sur le site de l'AFPy pour présenter le CD (pas prio) -> en cours
   * Marc \& Antoine : trouver un bar pour une vingtaine de personnes pour accueillir les gens le vendredi soir de la PyConFR -> fait
       * Antoine : contacter la lanterne pour réserver une table pour 20 personnes le vendredi soir de la PyConFR -> fait (réservé pour le 01/11 19h30 à la lanterne pour une vingtaine de personnes)
   * Lucie / Guillaume : faire un PDF du planning -> fait, en prod
   * Hervé : savoir la deadline pour envoyer les nombres de personnes au JRES
   * Antoine : contacter les paniers de Léa pour voir si on peut faire quelque chose pour l'anniversaire -> fait, pas de réponse, relancés
       * Voir ce qu'il est possible (brownie sans noix ? flan ? tartes ?) et pour combien de personnes (150 ?)
   * Laurine : faire affiche concernant l'équipe diversité
   * Melcore : se renseigner pour le numéro proxy -> fait [https://discuss.afpy.org/t/numero-de-telephone-pour-le-comite-diversite/2318](https://discuss.afpy.org/t/numero-de-telephone-pour-le-comite-diversite/2318)




## Ordre du jour

   * Utilisation d’un sujet par réunion du CD sur le Discuss, partage du compte-rendu, ordre du jour ([https://discuss.afpy.org/t/reunion-du-comite-directeur/1156/45)](https://discuss.afpy.org/t/reunion-du-comite-directeur/1156/45))
       * → garder un sujet unique avec événement récurrent, épingler le sujet
       * → poster systématiquement les compte-rendus sur les sujets
       * → annoncer sur le sujet que les membres peuvent proposer des sujets pour la prochaine réunion, mais doivent venir à la réunion en question pour les "soutenir"
   * Matériel ([https://discuss.afpy.org/t/materiel-achat-ou-location/2309)](https://discuss.afpy.org/t/materiel-achat-ou-location/2309))
       * louer des percolateurs pour les sprints (dimanche aprem ?)
   * Courses ([https://discuss.afpy.org/t/pyconfr2024-liste-de-courses/2320)](https://discuss.afpy.org/t/pyconfr2024-liste-de-courses/2320))
   * Atelier pour les enfants (on est lundi soir et y a pas de neeeews, toujours pas mardi)
       * → trop tard
   * Paiement Annabelle
   * Système de vote si jamais plus de 3 personnes veulent rejoindre le CD
       * Vote de valeur, et on verra les modalités (urnes ? électronique ?) sur le moment


## Actions

   * Lucie : prendre un abonnement pour 1 mois de numéro de téléphone proxy pour la PyConFR
   * Antoine : corriger les liens github→gitea sur le site de l'AFPy
   * Antoine : rouvrir un sujet récurrent pour les réunions CD, après la PyConFR
   * Antoine : s'occuper de contacter kiloutou pour la location de 2 percolateurs 7L pour le jeudi-vendredi
   * Antoine : rappeler les foodtrucks pour donner l'évolution du nombre de participant·e·s
   * Lucie : contacter la responsable de l'atelier pour les enfants pour annoncer que c'est trop tard




## Actions reportées indéfiniment

   * Thomas : se renseigner pour savoir si l’association peut avoir le statut d’intérêt général
   * debnet : Prendre contact avec Gandi pour l'interview et obtenir les questions → voir une occasion pour aller sur Paris et coupler avec Gandi
   * Pierre : se renseigner auprès d'une maison d'asso sur les aspects juridiques concernant la réception de donation / rémunération d'intervenant·e·s -> Relancé le 16/10/23, pas de réponse -> Toujours pas de réponse (au 20/02/24)


Prochaine réunion le 3 novembre après l'AGO



**Fin : 20hXX**
