Suite au dernier rejet je me permets de joindre ce courrier explicatif des dernières modifications.

L'Assemblée générale de l'AFPY s'est tenue le 9 février dernier selon les statuts en vigueur depuis 2018.
La première déclaration du compte-rendu de cette AG a été refusée parce que nous avions oublié de vous soumettre les nouveaux statuts en 2018 et que l'AG n'était pas conforme aux statuts plus anciens.

J'ai donc inclus les statuts en question dans une seconde déclaration qui a été rejetée pour plusieurs remarques auxquelles j'aimerais ici répondre.

Le siège social n'a pas été modifié, l'association est toujours domicilée au 2 rue Professeur Zimmermann, 69007 Lyon.
Cependant cette adresse est maintenant stipulée dans le réglement général (que je joins ici) plutôt que dans les statuts.

L'objet a été quelque peu affiné et je vais donc le préciser lors de cette nouvelle déclaration.

Ont été modifiés dans les statuts les articles 1 à 9, un nouvel article article 10 a été inséré (décalant le précédent article 10 à 11, de même pour les articles suivants), ont aussi été modifiés les articles 11 à 13 (anciennement 10 à 12) et les articles 15 à 21 (anciennement 14 à 20) ainsi que l'article 25 (anciennement 24).
Les articles 21, 22, 23 et 25 ont été supprimés, remplacés par de nouveaux articles 22, 23 et 24.

La composition du breau est précisée dans le réglement général et leur mandat est d'un an (jusqu'à la nouvelle composition du bureau parmi le comitant dirigeant lors de la nouvelle assemblée générale ordinaire).
Le mandat des membres du CD n'est pas spécifié explicitement, simplement un tiers du comité est renouvelé chaque année (affectant prioritairement les membres les plus anciens s'il n'y a pas de démissionnaires). Un mandat dure alors classiquement trois ans.

Cordialement.
