Bonjour,

Je fais suite au rejet de la déclaration pour apporter quelques précisions dans la nouvelle version jointe.

- Le PV a été modifié afin de corriger la date et d'ajouter les modifications précises apportées aux statuts.
- Les noms est fonctions des 9 membres du comité directeur y étaient déjà décrites.
- La composition précise du bureau est décrite dans le règlement intérieur que je joins à cette nouvelle déclaration.

Nous sommes actuellement bloqués depuis plus d'un an sur cette déclaration parce qu'avons dû convoquer une nouvelle assemblée générale pour apporter les modifications nécessaires, nous aimerions si possible pouvoir éviter de recommencer.

Cordialement,

Antoine Rozo.
