++ SociétéGénérale (Order,Transfer,Cash,Check): 13361.08
++ PayPal (PayPal): 2617.41

-- Janvier


2023/01/05 -42.00€ Order Domiciliation X

2023/01/05 -397.94€ Order Domiciliation X
    MAIF

2023/01/10 5412.60€ PayPal PyconFr2023 X
    PSF grant (6000 $)

2023/01/11 1208.00€ Transfer Cotisation X
    Cotisation + PyConFr

2023/01/16 1000.00€ Transfer PyconFr2023 X
    CGWIRE

2023/01/20 2000.00€ Transfer PyconFr2023 X
    GITGUARDIAN

2023/01/23 -200.00€ Transfer PyconFr2023 X
    BOURSE

2023/01/23 -174.10€ Transfer PyconFr2023 X
    BOURSE

2023/01/25 -13.00€ Order Banque X

2023/01/31 -2262.00€ Transfer PyconFr2023 X
    TSHIRT MISTERUGBY

2023/01/31 -275.93€ Transfer PyconFr2023 X
    ECOCUP REUZ

2023/01/31 2000.00€ Transfer PyconFr2023 X
    SPONSORING ALMA

2023/01/31 500.00€ Transfer PyconFr2023 X
    SPONSORING Mozilla

2023/01/31 -403.98€ PayPal PyconFr2023 X
    BOURSE

-- Février

2023/02/04 -42.00€ Order Domiciliation X

2023/02/06 -1213.92€ Transfer PyconFr2023 X
    TOUR DE COUP AFPY

2023/02/06 -377.88€ Transfer PyconFr2023 X
    BOURSE

2023/02/09 -4999.32€ Transfer PyconFr2023 X
    DEJEUNER PANIER DE LEA

2023/02/10 3508.00€ Transfer Cotisation X
    Cotisation + PyConFr

2023/02/13 1000.00€ PayPal PyconFr2023 X
    SPONSORING MICHELIN

2023/02/13 -29.35€ PayPal PyconFr2023 X
    FRAIS PAYPAL

2023/02/13 2000.00€ Transfer PyconFr2023 X
    SPONSORING Clevercloud

2023/02/13 -400.00€ Transfer PyconFr2023 X
    BOURSE

2023/02/13 -200.00€ Transfer PyconFr2023 X
    BOURSE

2023/02/20 20.00€ Transfer PyconFr2023 X
    Don TSHIRT

2023/02/21 -31.20€ Transfer PyconFr2023 X
    REMOURSEMENT Casino Bordeaux

2023/02/21 -90.00€ Transfer PyconFr2023 X
    REMBOURSEMENT COPYFAC

2023/02/21 -200.00€ Transfer PyconFr2023 X
    BOURSE

2023/02/21 -200.00€ Transfer PyconFr2023 X
    REMBOURSEMENT Fond de caisse

2023/02/21 -509.25€ Transfer PyconFr2023 X
    REMBOURSEMENT Fond de caisse

2023/02/23 -13.00€ Order Banque X

2023/02/24 -28€ Transfer PyconFr2023 X
    REMBOURSEMENT COPIFAC

2023/02/24 -288.70€ Transfer PyconFr2023 X
    REMBOURSEMENT REPAS AFPY

2023/02/27 90€ Transfer PyconFr2023 X
    REVERSEMENT

2023/02/28 -316.50€ Transfer PyconFr2023 X
    PAIEMENT Panier de LEA FACTURE 230220-5563

-- Mars

2023/03/01 1607.01€ Transfer PyconFr2023 X
    CAISSE AFPY VENTE SUR PLACE

2023/03/01 -2142.56€ Transfer PyconFr2023 X
    DEJEUNER PANIER DE LEA SOLDE

2023/03/02 500.00€ Transfer PyconFr2023 X
    SPONSORING Gandi

2023/03/03 -42.00€ Order Domiciliation X

2023/03/06 -30.00€ Transfer PyconFr2023 X
    REMBOURSEMENT PLACE

2023/03/06 -1771.00€ Transfer PyconFr2023 X
    PAIEMENT REPAS MARIE CURRY

2023/03/08 -2340.00€ Transfer PyconFr2023 X
    PAIEMENT REPAS ENTR-AUTRES

2023/03/10 2065.00€ Transfer PyconFr2023 X
   HELLOASSO Cotisation et paiement

2023/03/13 -55.60€ Transfer PyconFr2023 X
    REMBOURSEMENT CASINO PILES

2023/03/13 -153.72€ Transfer PyconFr2023 X
    PAIEMENT BADGES

2023/03/13 -3888.05€ Transfer PyconFr2023 X
    PAIEMENT CAPTATION VIDEO

2023/03/17 -20.63€ PayPal Meetup X
    SPONSORING BOISSON AFPyro

2023/03/27 -13.00€ Order Banque X

-- Avril

2023/04/03 -216.00€ Transfer PyconFr2023 X
    PAIEMENT LIVESTREAM

2023/04/05 -42.00€ Order Domiciliation X

2023/04/12 145.00€ Transfer Cotisation X
   Cotisation

2023/04/24 -13.00€ Order Banque X

-- Mai

2023/05/04 -42.00€ Order Domiciliation X

2023/05/10 20.00€ Transfer Cotisation X
   Cotisation

2023/05/25 -13.00€ Order Banque X

-- Juin

2023/06/05 -2377.72€ Transfer PyconFr2023 X
   LOCATION UNIVERSITE

2023/06/05 -42.00€ Order Domiciliation X

2023/06/27 -13.00€ Order Banque X

-- Juillet

2023/07/04 -42.00€ Order Domiciliation X

2023/07/26 -13.00€ Order Banque X

2023/07/27 -432.00€ Transfer PyconFr2023 X
   HEBERGEMENT VIDEO Octopuce

-- Aout

2023/08/03 -42.00€ Order Domiciliation X

2023/08/09 70.00€ Transfer Cotisation X
   Cotisation

2023/08/15 -100.00€ Transfer APRIL X
   Cotisation APRIL

2023/08/26 -13.00€ Order Banque X

-- Septembre

2023/09/04 -42.00€ Order Domiciliation X

2023/09/06 20.00€ Transfer Cotisation X
   Cotisation

2023/09/26 -13.00€ Order Banque X

-- Octobre

2023/10/03 -42.00€ Order Domiciliation X

2023/10/06 20.00€ Transfer Cotisation X
   Cotisation

2023/10/26 -13.00€ Order Banque X

-- Novembre

2023/11/03 -42.00€ Order Domiciliation X

2023/11/25 -13.00€ Order Banque X

-- Décembre

2023/12/04 -42.00€ Order Domiciliation X

2023/12/05 10.00€ Transfer Cotisation X
   Cotisation

2023/12/23 -13.00€ Order Banque X
