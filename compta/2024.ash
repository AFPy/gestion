++ SociétéGénérale (Order,Transfer,Cash,Check): 3812.70
++ PayPal (PayPal): 8576.05

-- Janvier

2024/01/02 -42.00€ Order Domiciliation X

2024/01/08 -421.42€ Order Domiciliation X
    MAIF

2024/01/08 20€ Transfer Cotisation X
    Cotisation

2024/01/26 -13.77€ Order Banque X

-- Fevrier

2024/02/05 -42.00€ Order Domiciliation X

2024/02/05 1230.42€ Transfer Cotisation X
    Cotisation

2024/02/24 -13.00€ Order Banque X

2024/02/27 0.77€ Transfer Banque X

-- Mars
2024/03/05 -42.00€ Order Domiciliation X

2024/03/06 210.00€ Transfer Cotisation X
    Cotisation

2024/03/08 1000.00€ Transfer PyConFR2024 X
2024/03/14 500.00€ Transfer PyConFR2024 X
2024/03/19 500.00€ Transfer PyConFR2024 X

2024/03/26 -13.00€ Order Banque X

-- Avril
2024/04/04 -42.00€ Order Domiciliation X
2024/04/09 70.00€ Transfer Cotisation X
2024/04/25 -13.00€ Order Banque X

-- Mai
2024/05/03 -42.00€ Order Domiciliation X
2024/05/07 60.00€ Transfer Cotisation X
2024/05/13 5000.00€ Transfer PyConFR2024 X
2024/05/28 -13.00€ Order Banque X
2024/05/30 1000.00€ Transfer PyConFR2024 X

-- Juin
2024/06/03 -42.00€ Order Domiciliation X
2024/06/06 500.00€ Transfer PyConFR2024 X
2024/06/07 50.00€ Transfer Cotisation X
2024/06/18 -76.81€ Transfer Meetup X
2024/06/18 500.00€ Transfer PyConFR2024 X
2024/06/25 -13.00€ Order Banque X
2024/06/26 20.00€ Transfer Cotisation X
2024/06/28 300.00€ Transfer PyConFR2024 X

-- Juillet
2024/07/02 500€ Transfer PyConFR2024 X
2024/07/03 -42.00€ Order Domiciliation X
2024/07/09 20.00€ Transfer Cotisation X
2024/07/16 1000€ Transfer PyConFR2024 X
2024/07/26 -13.00€ Order Banque X

-- Août
2024/08/02 -42.00€ Order Domiciliation X
2024/08/08 90.00€ Transfer Cotisation X
2024/08/16 -100€ Transfer APRIL X
2024/08/21 500€ Transfer PyConFR2024 X
2024/08/27 -13.00€ Order Banque X

-- Septembre
2024/09/03 -42.00€ Order Domiciliation X
2024/09/10 1810.00€ Transfer Cotisation X
2024/09/11 -5000.01€ Transfer PyConFR2024 X
2024/09/11 -765€ Transfer PyConFR2024 X
2024/09/12 2000.00€ Transfer PyConFR2024 X
2024/09/23 7362.00€ PayPal PyConFR2024 X
    PSF grant (8000 $)
2024/09/25 -13.00€ Order Banque X
2024/09/26 2468.50€ Transfer PyConFR2024 X

-- Octobre
2024/10/02 -42.00€ Order Domiciliation X
2024/10/04 -1578.00€ Transfer PyConFR2024 X
2024/10/08 1924.00€ Transfer Cotisation X
2024/10/18 -142.00€ Transfer PyConFR2024 X
2024/10/18 -35.00€ Transfer PyConFR2024 X
2024/10/23 500.00€ Transfer PyConFR2024 X
2024/10/24 422.05€ PayPal PyConFR2024 X
2024/10/26 -13.00€ Order Banque X
2024/10/28 -30.00€ Transfer PyConFR2024 X
2024/10/30 300.00€ Transfer PyConFR2024 X


-- Novembre

